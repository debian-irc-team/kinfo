# kinfo: Searching Debian Linux Kernel Packages

The Debian Linux kernel packages contain information that may be of interest for user-support and hardware-compatibility information:

 - kernel configuration options
 - module metadata for PCI-Id matches
 - module metadata for firmware loading

These data can be cross-referenced against:

 - Debian Wiki: list of pages for kernel modules
 - firmware package contents

# Requirements

This module requires the following modules either from Debian packages or from PyPI. The package is normally tested with the current Debian unstable release and also the current stable release.

For routine operation:

```
# apt install python3-requests python3-bs4 python3-psycopg python3-psutil postgresql-17 postgresql-client-17 postgresql-17-debversion kmod pciutils rsync
# pip install pydebcontents
```

For testing:

```
# apt install python3-pytest python3-pytest-cov
# pip install pytest-postgresql
```

Notes:

 - for `pip install` you should be working inside a venv, or you will need the `--break-system-packages` option, with the downsides that brings to your local machine.)
 - psycopg version 3 (not psycopg2) is used.


# Installation

From git, having installed the dependencies:

```
$ git clone https://salsa.debian.org/debian-irc-team/kinfo
$ cd kinfo
$ pip install .
```

Prior to search queries being made:

  - a postgresql installation is needed with a database created for data storage
  - a configuration file needs to be constructed
  - the necessary files (`linux-image-*.deb`, `Contents`) need to be downloaded; the `sync` subcommand of the `kinfo-bulk-importer` script can do this
  - the data needs to be loaded into the database; this can be done by feeding individual data files to the importer, or via the `kinfo-bulk-importer` script's `import` subcommand (recommended).

## Configuration

The configuration file must have a `[database]` section to provide access to the postgresql database for loading the data.

If the bulk-importer is used (recommended), then the `[bulk-importer]` section needs to have details of a suitable mirror, and the releases and architectures that should be used.

The package contains a sample config file in `./kinfo/tests/test_kinfo.conf` that can be copied as a starting point.
The format is as used by the Python `configparser` module.

If desired, the `kinfo-search` tool can be provided with a different config file that gives only read access to the database, with the `kinfo-import` or `kinfo-bulk-importer` tools being given read-write access.

Configuration is loaded in the following order:
 1. `/etc/kinfo.conf`  (system-wide settings)
 1. `~/.config/kinfo.conf` (user settings)
 1. `./kinfo.conf` (current 'task' settings from current working directory)
 1. file specified on command line
 1. file specified in the `KINFO_CONFIG` environment variable

The first three files are only read if they exist and it is not an error if they do not exist.
It is an error for explicitly specified files (environment, command line) to not exist.
The values in later files overwrite the values obtained from previous files.


## Data import

Caution: this step requires approximately 7 GB of disk space for the available kernels.

The `kinfo-bulk-importer` is used to first download the relevant .deb, Packages, and Contents files from an archive mirror via `rsync`:

```
$ kinfo-bulk-importer --verbose --config kinfo-importer.conf sync
```

The data can then be imported into the database:

```
$ kinfo-bulk-importer --verbose --config kinfo-importer.conf import
```


# Command-line Usage

The module comes with a simple command-line interface for searches (`kinfo-search`) as well as Python modules for programmatic access to the data.

`kinfo-search` has a number of subcommands:

 - `kernels`: what has been indexed
 - `modinfo`: info about a module
 - `pciid`: what modules and firmware are relevant for a PCI device
 - `lspci`: look at PCI devices on local machine and match against hardware support
 - `pciname`: search PCI database for a name
 - `firmware`: show package name for a firmware file
 - `kconfig`: show kernel compilation options that were used

`--help` for each subcommand provides options specific to that subcommand.



## List indexed kernels

The list of kernels that has been displayed can be listed.

```
$ kinfo-search kernels --arch ARCH --release RELEASE
```

The output includes (as a fixed width table):
  - the Debian release
  - the kernel version (e.g. `uname -v` and the Debian package version)
  - the kernel release (e.g. `uname -r` and as included in the Debian package name)

```
$ kinfo-search kernels
sid                  6.5.13-1                       6.5.0-5-amd64
bullseye             5.10.197-1                     5.10.0-26-amd64
buster-backports     5.10.127-2~bpo10+1             5.10.0-0.deb10.16-amd64
trixie               6.5.10-1                       6.5.0-4-amd64
bullseye-backports   6.1.55-1~bpo11+1               6.1.0-0.deb11.13-amd64
bookworm             6.1.55-1                       6.1.0-13-amd64
buster               4.19.249-2                     4.19.0-21-amd64
```

The architecture can be optionally specified (e.g. `--arch arm64`) and
a reduced set or releases included in the output (e.g. `--release sid`).


## Kernel module info

Information about one or more modules is printed:

```
$ kinfo-search modinfo --arch ARCH --release RELEASE module
```

The output includes the list of PCI-Ids that the module claims to support, the list of firmware files it can load from userspace, the packages that include that firmware (if it is packaged), and any pages on the Debian Wiki that are marked as relevant to the module.

```
$ kinfo-search modinfo iwlwifi
Module: iwlwifi
Devices:
    00008086:0000E440
    00008086:00007740
    ...
Firmware:
    iwlwifi-1000-5.ucode                     firmware-iwlwifi
    iwlwifi-100-5.ucode                      firmware-iwlwifi
    ...
    iwlwifi-bz-a0-fm4-b0-83.ucode
    ...
Wiki:
    https://wiki.debian.org/iwlwifi
```

## PCI-Id lookup

This is the reverse lookup of the `modinfo` lookup, finding kernel modules that support the device and the firmware packages that might be relevant to it based on the module.

```
$ kinfo-search pciid --arch ARCH --release RELEASE --wildcards ABCD:1234
```

Some modules claim to support just about every device via wildcards, only to then not support them at runtime; `snd-hda-intel`, `ata_generic` are common culprits for this.
These wildcard matches are suppressed in the output by default.

PCI-Id searching is case insensitive and will accept both 8-digit and 4-digit codes for vendor and device Id fields.

```
$ kinfo-search pciid 8086:24f3
PCI-Id 8086:24f3 is a 'Intel Corporation' by 'Wireless 8260'
PCI-Id 8086:24f3 is claimed by modules in sid/amd64:
  iwlwifi
which will load firmware from packages:
  firmware-iwlwifi
The modules are referenced by the wiki pages:
  https://wiki.debian.org/iwlwifi
```

## Local device interrogation

It's possible to use lspci on the local machine to see what hardware is present and how it is supported.

```
$ kinfo-search lspci --arch ARCH --release RELEASE --modules --firmware
```

List the devices

```
$ kinfo-search lspci
...
[8086:156f] Intel Corporation: Ethernet Connection I219-LM
[10ec:522a] Realtek Semiconductor Co., Ltd.: RTS522A PCI Express Card Reader
[8086:24f3] Intel Corporation: Wireless 8260
...
```

List the devices *and* do a PCI-Id lookup on each of them:
```
$ kinfo-search lspci --modules --firmware
```
the kernel modules and firmware packages are added to each line. These two items can be combined.

```
$ kinfo-search lspci --modules
...
[8086:156f] Intel Corporation: Ethernet Connection I219-LM; modules: e1000e
[10ec:522a] Realtek Semiconductor Co., Ltd.: RTS522A PCI Express Card Reader; modules: rtsx_pci
[8086:24f3] Intel Corporation: Wireless 8260; modules: iwlwifi
...
```

```
$ kinfo-search lspci --firmware
...
[8086:156f] Intel Corporation: Ethernet Connection I219-LM
[10ec:522a] Realtek Semiconductor Co., Ltd.: RTS522A PCI Express Card Reader
[8086:24f3] Intel Corporation: Wireless 8260; firmware: firmware-iwlwifi
...
```


## PCI device name searches

The full name of the hardware in the PCI database and the associated PCI-Id can be discovered from this command.


```
$ kinfo-search pciname fragment
```

For example:

```
$ kinfo-search pciname wimax
[00008086:00000087] Intel Corporation: Centrino Advanced-N + WiMAX 6250 [Kilmer Peak]
[00008086:00000089] Intel Corporation: Centrino Advanced-N + WiMAX 6250 [Kilmer Peak]
[00008086:00000885] Intel Corporation: Centrino Wireless-N + WiMAX 6150
[00008086:00000886] Intel Corporation: Centrino Wireless-N + WiMAX 6150
[00008086:0000423C] Intel Corporation: WiMAX/WiFi Link 5150
[00008086:0000423D] Intel Corporation: WiMAX/WiFi Link 5150
```

Searches are case-insensitive substring matches.


## Firmware package searches

The Debian packages that contain specified firmware filenames can be identified. This is basically the same as an `apt-file` search except that all the files landing in firmware have been put into a database in advance.
The database is loaded with firmware in both traditional (`/lib/firmware`) and merged-/usr (`/usr/lib/firmware`) locations.

```
$ kinfo-search firmware --arch ARCH --release RELEASE filename
```

For example:

```
$ kinfo-search firmware iwlwifi-135-6.ucode
Firmware iwlwifi-135-6.ucode can be found in 1 packages:
  firmware-iwlwifi
```



## Kernel config searches

The kernel configuration that was used to compile the package can be searched, based on the `/boot/config-*` file that is included in the package.

```
$ kinfo-search firmware --arch ARCH --release RELEASE fragment
```

`CONFIG_` is automatically prepended to the search term if it is missing; it is omitted from the output. The `%` character can be used as a wildcard for the search.

The output includes:
 - `y` for enabled options
 - `m` for enabled as a module
 - `n` for disabled options (based on the `# CONFIG_FOO is not set` comments in the config file)


```
$ kinfo-search kconfig CIFS
CIFS                          : m
```

```
$ kinfo-search kconfig CONFIG_CIFS_DEBUG%
CIFS_DEBUG                    : y
CIFS_DEBUG2                   : n
CIFS_DEBUG_DUMP_KEYS          : n
```


# Python Usage

From Python, the `kinfo.search` module contains functions for programmatic access to each of the above searches.

For example, the `pciid` subcommand above is based on the following code:

```python
import kinfo.db
import kinfo.search

db = kinfo.db.KinfoDB()
with db.connection() as conn:
     modules, packages = kinfo.search.fetch_pciid_match(conn, "sid", "amd64", "8086:24f3")

print(modules)
# ['ahci', 'ata_generic', 'iwlwifi', 'snd_hda_intel']
print(packages)
# ['firmware-iwlwifi']
```

Note that the command-line interface has lots of reasonable default values embedded within it: it is often possible to not specify the release and architecture, for instance, with `sid` and `amd64` being used implicitly.
The module interface tends to require these to be specified;
cleansing of user input data should also occur *prior* to use;
functions like `kinfo.search.clean_pciid_options()` are provided to assist with this.

The functions in the `kinfo.search_cli` provide useful templates for using `kinfo.search`.


## To-do list / limitations

 - out-of-tree modules provided by `dkms` are not imported
 - additional hardware information contained in the Packages file is not imported
 - look at isenkram for some more ideas
