# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Search for information from the kernel info database
"""
##############################################################################

# TODO

##############################################################################

import logging
import subprocess

from typing import (
    Iterable,
    Literal,
    Optional,
    Union,
    overload,
)

from psycopg import Connection

from kinfo import search


logger = logging.getLogger(__name__)

wiki_base = "https://wiki.debian.org/%s"

##############################################################################


__module_print_format = """\
Module:\t%(module)s
Devices:
\t%(devices)s
Firmware:
%(firmware)s
Wiki:
%(wiki_pages)s
"""


def show_modinfo(
    conn: Connection,
    release: str,
    architecture: str,
    modules: Union[str, Iterable[str]],
) -> bool:
    """
    display information on a given module
    """
    if isinstance(modules, str):
        modules = [modules]
    failures = 0
    for module in modules:
        logger.debug("show_modinfo: module: %s", module)
        try:
            devices = search.module_lookup(conn, release, architecture, module)
        except ValueError:
            failures += 1
            continue

        firmware = []
        firmware_files = search.firmware_lookup(conn, release, architecture, module)
        for f in firmware_files:
            packages = search.firmware_packages_lookup(conn, release, architecture, f)
            firmware.append("\t%-40s %s" % (f, " ".join(packages)))

        wiki_pages = search.wiki_match_lookup(conn, module)
        wiki_pages = ["\t%s" % (wiki_base % w) for w in wiki_pages]

        print(
            __module_print_format
            % {
                "module": module,
                "devices": "\n\t".join("%s:%s" % (d[0], d[1]) for d in devices),
                "firmware": "\n".join(firmware),
                "wiki_pages": "\n".join(wiki_pages),
            }
        )

    return failures == 0


def show_firmware(
    conn: Connection,
    release: str,
    architecture: str,
    filenames: Union[str, Iterable[str]],
) -> bool:
    """ display information on location of some firmware files """
    if isinstance(filenames, str):
        filenames = [filenames]

    failures = 0
    for filename in filenames:
        logger.debug("show_firmware: filename: %s", filename)
        try:
            packages = search.firmware_packages_lookup(
                conn, release, architecture, filename
            )
        except ValueError as e:
            print("show_firmware: Error finding data: %s" % e)
            failures += 1
            continue

        if packages:
            print(
                "Firmware %s can be found in %d packages:" % (filename, len(packages))
            )
            for p in packages:
                print("  %s" % p)
        else:
            print("Firmware %s cannot be found in any packages." % filename)
            failures += 1

    return failures == 0


def show_pciid(
    conn: Connection,
    release: str,
    architecture: str,
    pciids: Union[str, Iterable[str]],
    wildcards: bool = False,
) -> bool:
    """ display information on each device specified """
    if isinstance(pciids, str):
        pciids = [pciids]

    failures = 0
    for pciid in pciids:
        logger.debug("show_pciid: PCI Id: %s", pciid)
        if not search.pciid_check_format(pciid):
            print("Error: PCI Id '%s' is not of an acceptable form")
            failures += 1
            continue

        vname, dname = search.pciid_name_lookup(conn, pciid=pciid)
        if vname is not None and dname is not None:
            print(f"PCI-Id {pciid} is a '{vname}' by '{dname}'")
        else:
            print(f"PCI-Id {pciid} was not in the PCI Id database")

        try:
            modules, packages = search.fetch_pciid_match(
                conn, release, architecture, pciid, wildcards
            )
        except ValueError as e:
            print("Error looking up data: %s" % e)
            failures += 1
            continue

        if modules:
            print(
                "PCI-Id %s is claimed by modules in %s/%s:\n"
                "  %s" % (pciid, release, architecture, " ".join(modules))
            )
            if packages:
                print(
                    "which will load firmware from packages:\n"
                    "  %s" % (" ".join(set(packages)))
                )
        else:
            print(
                "PCI-Id %s is not claimed by any modules in %s/%s."
                % (pciid, release, architecture)
            )
            failures += 1

        wiki_pages = search.wiki_match_lookup(conn, modules)
        if wiki_pages:
            print(
                "The modules are referenced by the wiki pages:\n",
                "\n".join("  %s" % (wiki_base % page) for page in wiki_pages),
            )

    return failures == 0


def show_kernels_summary(conn: Connection, release: str, architecture: str) -> bool:
    """Print a list of all know kernels

    returns:
        True if kernels were found
    """
    kernels = search.kernel_lookup(conn, release, architecture)
    for k in kernels:
        print("%-20s %-30s %-20s" % (k["release"], k["version"], k["uname"]))

    return len(kernels) > 0


def show_kconfig(
    conn: Connection,
    release: str,
    architecture: str,
    kconfigs: Union[str, Iterable[str]],
) -> bool:
    """Print selected kconfig entries for a kernel

    returns:
        True if the kconfig items were all found
    """
    if isinstance(kconfigs, str):
        kconfigs = [kconfigs]

    failures = 0
    for kconfig in kconfigs:
        logger.debug("show_kconfig: KConfig: %s", kconfig)
        try:
            keys = search.kconfig_lookup(conn, release, architecture, kconfig)
        except ValueError as e:
            print("Error looking up data: %s" % e)
            failures += 1
            continue

        if not keys:
            failures += 1
        for k in keys:
            print("%-30s: %s" % (k[0], k[1]))

    return failures == 0


def show_pciname_matches(conn: Connection, name: str) -> bool:
    """Print PCI device names matching a search term

    returns:
        True if devices were found
    """
    if "%" not in name:
        name = f"%{name}%"

    devices = search.pciname_match_lookup(conn, name)
    for dev in devices:
        print("[%s:%s] %s: %s" % (dev[0], dev[1], dev[2], dev[3]))

    return len(devices) > 0


@overload
def show_lspci(
    conn: Connection, release: str, arch: str, modules: Literal[True], firmware: bool
) -> bool:
    pass


@overload
def show_lspci(
    conn: Connection, release: str, arch: str, modules: bool, firmware: Literal[True]
) -> bool:
    pass


@overload
def show_lspci(
    conn: Connection,
    release: Optional[str],
    arch: Optional[str],
    modules: bool,
    firmware: bool,
) -> bool:
    pass


def show_lspci(
    conn: Connection,
    release: Optional[str],
    arch: Optional[str],
    modules: bool,
    firmware: bool,
) -> bool:
    """Print PCI device names from current machine

    returns:
        True if devices were found
    """
    if (modules or firmware) and (release is None or arch is None):
        logger.error(
            "lspci: requested to show modules/firmware but release/arch "
            "not specified"
        )
        modules = False
        firmware = False

    wildcards = False

    listing = subprocess.check_output(["lspci", "-nmm"])

    for line in listing.splitlines():
        entries = line.decode("UTF-8").split()
        pciid = "%s:%s" % (entries[2][1:5], entries[3][1:5])
        logger.debug("lspci: looking for pciid=%s", pciid)

        vname, dname = search.pciid_name_lookup(conn, pciid=pciid)
        logger.debug("lspci: identified '%s' from '%s'", vname, dname)

        module_text = ""
        firmware_text = ""
        if (modules or firmware) and release is not None and arch is not None:
            try:
                modulelist, packagelist = search.fetch_pciid_match(
                    conn, release, arch, pciid, wildcards
                )
                logger.debug("lspci: modules=%s", modulelist)
                logger.debug("lspci: firmware packages=%s", packagelist)
                if modules and modulelist:
                    module_text = "; modules: " + ", ".join(modulelist)
                if firmware and packagelist:
                    firmware_text = "; firmware: " + ", ".join(packagelist)
            except ValueError as e:
                logger.warning("lspci:modules lookup error: %s", e)

        print("[%s] %s: %s%s%s" % (pciid, vname, dname, module_text, firmware_text))

    return bool(listing)
