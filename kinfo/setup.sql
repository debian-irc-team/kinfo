-- SQL scheme a for postgresql implementation of kernel info database
--
-- Part of the kinfo project
--
-- Copyright (c) 2014-2021  Stuart Prescott
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions, and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright notice,
--     this list of conditions, and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
--   * Neither the name of the author of this software nor the name of
--     contributors to this software may be used to endorse or promote products
--     derived from this software without specific prior written consent.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

DROP SCHEMA IF EXISTS kinfodb CASCADE;
CREATE SCHEMA kinfodb;

SET search_path TO kinfodb, public;

DROP TABLE IF EXISTS kinfodb.kernels;
CREATE TABLE kinfodb.kernels
    (
        id SERIAL PRIMARY KEY,
        version TEXT,
        uname TEXT,
        release TEXT,
        architecture TEXT
    );

DROP TABLE IF EXISTS kinfodb.modules;
CREATE TABLE kinfodb.modules
    (
        kernel_id INTEGER REFERENCES kinfodb.kernels(id) ON DELETE CASCADE,
        module TEXT
    );

DROP TABLE IF EXISTS kinfodb.pciids;
CREATE TABLE kinfodb.pciids
    (
        kernel_id INTEGER REFERENCES kinfodb.kernels(id) ON DELETE CASCADE,
        module TEXT,
        vid TEXT,
        did TEXT
    );

DROP TABLE IF EXISTS kinfodb.pci_devices;
CREATE TABLE kinfodb.pci_devices
    (
        vid TEXT,
        did TEXT,
        vendor TEXT,
        device TEXT
    );


DROP TABLE IF EXISTS kinfodb.kconfig;
CREATE TABLE kinfodb.kconfig
    (
        kernel_id INTEGER REFERENCES kinfodb.kernels(id) ON DELETE CASCADE,
        key TEXT,
        value TEXT
    );

DROP TABLE IF EXISTS kinfodb.firmware;
CREATE TABLE kinfodb.firmware
    (
        kernel_id INTEGER REFERENCES kinfodb.kernels(id) ON DELETE CASCADE,
        module TEXT,
        filename TEXT
    );

DROP TABLE IF EXISTS kinfodb.firmware_packages;
CREATE TABLE kinfodb.firmware_packages
    (
        release TEXT,
        architecture TEXT,
        package TEXT,
        filename TEXT
    );

DROP TABLE IF EXISTS kinfodb.wiki_map;
CREATE TABLE kinfodb.wiki_map
    (
        module TEXT,
        wikipage TEXT
    );
