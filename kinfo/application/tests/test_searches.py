# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

from typing import Any, Generator

import pytest

from kinfo.application import searches
from .test_base import _applicationTestBase


class TestApplicationSearch(_applicationTestBase):
    # pylint: disable=unused-argument

    @pytest.fixture(autouse=True)
    def argparser(self) -> Generator[None, None, None]:
        self._argparser = searches._search_args
        yield

    def test_search_help(self, argparser: Any) -> None:
        self.assertShowsHelp(["--help"])
        self.assertShowsHelp(["-h"])

    def test_search_verbose(self) -> None:
        args = searches._search_args(["kernels"])
        assert not args.verbose

        args = searches._search_args(["--verbose", "pciname", "foo"])
        assert args.verbose == 1

        args = searches._search_args(["-v", "pciname", "foo"])
        assert args.verbose == 1

        args = searches._search_args(["-v", "-v", "pciname", "foo"])
        assert args.verbose == 2

    def test_search_kernels(self, argparser: Any) -> None:
        self.assertShowsHelp(["kernels", "--help"])

        args = searches._search_args(["kernels"])
        assert callable(args.func)

        args = searches._search_args(["kernels", "--release", "s"])
        assert args.release == "s"

    def test_search_modinfo(self, argparser: Any) -> None:
        self.assertShowsHelp(["modinfo", "--help"])
        self.assertShowsHelpError(["modinfo"])

        args = searches._search_args(["modinfo", "--release", "s", "iwl"])
        assert callable(args.func)
        assert args.release == "s"
        assert args.modules == ["iwl"]

        args = searches._search_args(["modinfo", "iwl"])
        assert args.release == "sid"
        assert args.modules == ["iwl"]

        args = searches._search_args(["modinfo", "--release", "s", "iwl", "iwq"])
        assert callable(args.func)
        assert args.release == "s"
        assert args.modules == ["iwl", "iwq"]

    def test_search_pciid(self, argparser: Any) -> None:
        self.assertShowsHelp(["pciid", "--help"])
        self.assertShowsHelpError(["pciid"])
        self.assertShowsHelpError(["pciid", "--release", "s"])
        self.assertShowsHelpError(["pciid", "--arch", "a"])

        args = searches._search_args(["pciid", "1234:5678"])
        assert callable(args.func)
        assert args.release == "sid"
        assert args.arch == "amd64"
        assert args.pciids == ["1234:5678"]

        args = searches._search_args(
            ["pciid", "--release", "s", "--arch", "a", "1234:5678", "abcd:ef01"]
        )
        assert args.release == "s"
        assert args.arch == "a"
        assert args.pciids == ["1234:5678", "abcd:ef01"]

    def test_search_firmware(self, argparser: Any) -> None:
        self.assertShowsHelp(["firmware", "--help"])
        self.assertShowsHelpError(["firmware"])
        self.assertShowsHelpError(["firmware", "--release", "s"])
        self.assertShowsHelpError(["firmware", "--arch", "a"])

        args = searches._search_args(["firmware", "something.fw"])
        assert callable(args.func)
        assert args.release == "sid"
        assert args.arch == "amd64"
        assert args.firmware == ["something.fw"]

        args = searches._search_args(
            [
                "firmware",
                "--release",
                "s",
                "--arch",
                "a",
                "something.fw",
                "otherthing.fw",
            ]
        )
        assert args.release == "s"
        assert args.arch == "a"
        assert args.firmware == ["something.fw", "otherthing.fw"]

    def test_search_kconfig(self, argparser: Any) -> None:
        self.assertShowsHelp(["kconfig", "--help"])
        self.assertShowsHelpError(["kconfig"])
        self.assertShowsHelpError(["kconfig", "--release", "s"])
        self.assertShowsHelpError(["kconfig", "--arch", "a"])

        args = searches._search_args(["kconfig", "%CIFS%"])
        assert callable(args.func)
        assert args.release == "sid"
        assert args.arch == "amd64"
        assert args.kconfig == ["%CIFS%"]

        args = searches._search_args(
            ["kconfig", "--release", "s", "--arch", "a", "%CIFS%", "%SMB%"]
        )
        assert args.release == "s"
        assert args.arch == "a"
        assert args.kconfig == ["%CIFS%", "%SMB%"]

    def test_search_pciname(self, argparser: Any) -> None:
        self.assertShowsHelp(["pciname", "--help"])
        self.assertShowsHelpError(["pciname"])

        args = searches._search_args(["pciname", "Centrino"])
        assert callable(args.func)
        assert args.name == "Centrino"
