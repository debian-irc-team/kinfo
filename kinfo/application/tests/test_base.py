# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import argparse
import logging

from typing import Callable, List, Optional

import pytest

from kinfo.application import base
from kinfo.tests._util import _StdOutTestMixin


class _applicationTestBase(_StdOutTestMixin):
    _argparser: Callable[[Optional[List[str]]], argparse.Namespace]

    def assertShowsHelp(self, args: List[str]) -> None:
        with self.assertInStdOut(["usage"]):
            with pytest.raises(SystemExit):
                self._argparser(args)

    def assertShowsHelpError(self, args: List[str]) -> None:
        with self.assertInStdOut(err_needles=["usage"]):
            with pytest.raises(SystemExit):
                self._argparser(args)


class TestApplicationGeneral(_applicationTestBase):
    def test_setup_logging(self) -> None:
        # get the current level to be able to restore it later
        level = logging.getLogger().getEffectiveLevel()

        base.setup_logging(0)
        assert logging.getLogger().getEffectiveLevel() == logging.WARN
        logging.getLogger().handlers.pop()

        base.setup_logging(1)
        assert logging.getLogger().getEffectiveLevel() == logging.INFO
        logging.getLogger().handlers.pop()

        base.setup_logging(2)
        assert logging.getLogger().getEffectiveLevel() == logging.DEBUG
        logging.getLogger().handlers.pop()

        base.setup_logging(3)
        assert logging.getLogger().getEffectiveLevel() == logging.DEBUG

        logging.getLogger().setLevel(level)
        logging.getLogger().handlers.pop()
