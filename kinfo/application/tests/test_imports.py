# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

from typing import Any, Generator

import pytest

from kinfo.application import imports
from .test_base import _applicationTestBase


class TestApplicationImporter(_applicationTestBase):
    # pylint: disable=unused-argument

    @pytest.fixture(autouse=True)
    def argparser(self) -> Generator[None, None, None]:
        self._argparser = imports._importer_args
        yield

    def test_importer_help(self, argparser: Any) -> None:
        self.assertShowsHelp(["--help"])
        self.assertShowsHelp(["-h"])

    def test_importer_verbose(self) -> None:
        args = imports._importer_args(["pciids", "foo"])
        assert not args.verbose

        args = imports._importer_args(["--verbose", "pciids", "foo"])
        assert args.verbose == 1

        args = imports._importer_args(["-v", "pciids", "foo"])
        assert args.verbose == 1

        args = imports._importer_args(["-v", "-v", "pciids", "foo"])
        assert args.verbose == 2

    def test_importer_kernel(self, argparser: Any) -> None:
        self.assertShowsHelpError(["kernel"])
        self.assertShowsHelpError(["kernel", "foo.deb"])

        args = imports._importer_args(["kernel", "foo.deb", "--release", "sid"])
        assert callable(args.func)
        assert args.deb == "foo.deb"
        assert args.release == "sid"

        args = imports._importer_args(
            ["kernel", "foo.deb", "--release", "sid", "--release", "stretch"]
        )
        assert args.release == "stretch"

    def test_importer_contents(self, argparser: Any) -> None:
        self.assertShowsHelpError(["contents"])
        self.assertShowsHelpError(["contents", "/path/"])

        args = imports._importer_args(
            ["contents", "/path/", "--release", "sid", "--arch", "amd64"]
        )
        assert callable(args.func)
        assert args.tree == "/path/"
        assert args.release == ["sid"]
        assert args.arch == ["amd64"]

        args = imports._importer_args(
            [
                "contents",
                "/path/",
                "--release",
                "sid",
                "--release",
                "stretch",
                "--arch",
                "amd64",
                "--arch",
                "i386",
            ]
        )
        assert args.release == ["sid", "stretch"]
        assert args.arch == ["amd64", "i386"]

    def test_importer_pciids(self, argparser: Any) -> None:
        self.assertShowsHelpError(["pciids"])
        self.assertShowsHelpError(["pciids", "foo.ids", "--release", "sid"])

        args = imports._importer_args(["pciids", "foo.ids"])
        assert callable(args.func)
        assert args.pciids == "foo.ids"

    def test_importer_wikimap(self, argparser: Any) -> None:
        self.assertShowsHelpError(["wiki"])
        self.assertShowsHelpError(["wiki", "modules.wikimap", "--release", "sid"])

        args = imports._importer_args(["wiki", "modules.wikimap"])
        assert callable(args.func)
        assert args.wiki == "modules.wikimap"
