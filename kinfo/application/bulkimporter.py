# coding: utf-8
#
# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Bulk importer for all data
"""

import argparse
import contextlib
import gzip
import lzma
import logging
import itertools
import operator
from pathlib import Path
import re
import tempfile
import subprocess

from typing import (
    Dict,
    Generator,
    IO,
    Iterable,
    List,
    Optional,
    Tuple,
    TypedDict,
    Union,
    cast,
)

from debian import deb822
from psycopg import Connection
import requests

import kinfo.application.imports
import kinfo.application.base
import kinfo.db
import kinfo.config
import kinfo.importer


logger = logging.getLogger("kinfo-bulkimporter")

hash_methods = ["sha512", "sha256", "md5sum"]
timeout = 180  # seconds for requests.get

##################################################################


class ReleaseIndexDict(TypedDict, total=False):
    name: str
    size: str


class ResourceLocator:
    def __init__(self, filename: Path) -> None:
        with open(filename, 'rt', encoding="UTF-8") as fh:
            self.release = deb822.Release(fh.readlines())
        self.compressions = [".gz", "", ".xz"]

    def file_list_method(self) -> str:
        for h in hash_methods:
            if h in self.release.keys():
                logger.debug("ResourceLocator: identifying files by hash: %s", h)
                return h
        raise KeyError("Can't find a way to locate files in the Release index")

    def find_resource(self, pattern: str) -> Iterable[ReleaseIndexDict]:
        hash_method = self.file_list_method()
        all_files = self.release[hash_method]
        all_files_list = {f["name"]: f for f in all_files}
        targets = {}
        for c in self.compressions:
            variant = pattern + c
            if variant in all_files_list:
                targets[variant] = all_files_list[variant]

        return targets.values()

    def packages(self, component: str, arch: str) -> Iterable[ReleaseIndexDict]:
        pattern = f"{component}/binary-{arch}/Packages"
        return self.find_resource(pattern)


class KernelLocator:
    # linux-image-[uname]-[abi]-[flavour]
    # linux-image-[uname]-[abi]-[flavour]-dbg
    # linux-image-[flavour]
    # linux-image-[uname]-[abi]-[variant]-[flavour]

    kernelre = re.compile(
        r"""
        ^linux-image-
        (?P<uname>\d+\.\d[\d.]+)-       # kernel version 1.2.0
        (?P<abi>[^-]+)-                 # kernel ABI (digits or 'trunk')
        (?P<arch>.+?)                   # kernel architecture
        (?:-(?P<unsigned>unsigned))?    # optional -unsigned
        (?<!-dbg)                       # negative match for -dbg
        $""",
        re.VERBOSE,
    )

    def __init__(self, filename: Path, indexes: Iterable[ReleaseIndexDict]) -> None:
        self.filename = filename
        self.indexes = indexes

    def _find_best_index(self) -> Path:
        for idx in self.indexes:
            path = self.filename / idx["name"]
            if path.exists() and path.is_file():
                logger.info("KernelLocator: found index %s", path)
                return path
            logger.debug("KernelLocator: didn't find index %s", path)
        raise OSError(f"resource not found: {self.filename}")

    @staticmethod
    @contextlib.contextmanager
    def _auto_decompressor(filename: Path) -> Generator[IO[bytes], None, None]:
        cfile: IO[bytes]
        try:
            if filename.name.endswith("Packages"):
                logger.debug("IndexDecompressor: using uncompressed file")
                cfile = open(filename, "rb")

            elif filename.name.endswith(".gz"):
                logger.debug("IndexDecompressor: using gzip")
                cfile = cast(IO[bytes], gzip.open(filename, "rb"))

            elif filename.name.endswith(".xz"):
                logger.debug("IndexDecompressor: using xz via tempfile")
                # apt_pkg can't cope with the filehandles from lzma so use a tempfile
                tmpfile = tempfile.NamedTemporaryFile()
                cfile = lzma.open(filename, "rb")
                tmpfile.write(cfile.read())
                cfile.close()
                tmpfile.seek(0)
                cfile = tmpfile

            else:
                raise ValueError(f"Couldn't figure out how to open resource {filename}")

            yield cfile

        finally:
            if cfile:
                cfile.close()

    def find_kernels(self) -> Iterable[deb822.Packages]:
        path = self._find_best_index()
        with self._auto_decompressor(path) as fh:
            packages = deb822.Packages.iter_paragraphs(fh, use_apt_pkg=True)
            candidates = {
                p["package"]: p for p in packages if self.kernelre.match(p["package"])
            }
        targets = []
        for name, package in candidates.items():
            if "-unsigned" in name and name.replace("-unsigned", "") in candidates:
                continue
            targets.append(package)
        targets = sorted(
            targets, key=operator.methodcaller("get_version"), reverse=True
        )
        for t in targets:
            logger.debug("find_kernels: found %s=%s", t["package"], t["version"])
        return targets

    def filter_kernels(
        self,
        kernels: Iterable[deb822.Packages],
        kernel_filter: Optional[Tuple[str, str]],
    ) -> Iterable[deb822.Packages]:
        if kernel_filter is None:
            return kernels

        pos = kernel_filter[0]
        posre = re.compile(pos)
        neg = kernel_filter[1]
        negre = re.compile(neg)

        found_version = None

        filtered_kernels = []
        for k in kernels:
            matches = self.kernelre.search(k["package"])
            if not matches:
                logger.debug(
                    "filter_kernels: kernel %s doesn't match so skipping", k["package"]
                )
                continue

            groups = matches.groupdict()
            new_found_version = f"{groups['uname']}-{groups['abi']}"

            if found_version and found_version != new_found_version:
                # rolled into an older kernel release
                logger.debug(
                    "filter_kernels: rest are older kernel version so skipping"
                )
                break
            found_version = new_found_version

            if neg and negre.search(groups["arch"]):
                logger.debug("filter_kernels: rejected %s", k["package"])
                continue

            if pos and posre.search(groups["arch"]):
                logger.debug("filter_kernels: accepted %s", k["package"])
                filtered_kernels.append(k)
                continue

            logger.debug("filter_kernels: passthru for %s", k["package"])

        if kernels and not filtered_kernels:
            logger.error("filter_kernels: managed to filter all of them out!")

        return filtered_kernels


##################################################################


def rsync_mirror_data(
    rsynchost: str,
    rsyncpath: str,
    cachebase: Union[str, Path],
    releases: Iterable[str],
    archs: Iterable[str],
) -> bool:

    host = f"{rsynchost}::{rsyncpath}"

    # Build rsync options that will include the archive indices that are
    # desired. Grab the .gz versions as they are the easiest to consume here.
    index_options = []
    for release, arch in itertools.product(releases, archs):
        index_options.extend(
            [
                f"--include=dists/{release}/*Release*",
                f"--include=dists/{release}/*/binary-{arch}/Packages*",
                f"--include=dists/{release}/*/Contents-{arch}.gz",
                f"--include=dists/{release}/*/Contents-all.gz",
            ]
        )

    # Build rsync options that will include the kernel images (signed and
    # unsigned).
    kernel_package_options = []
    for arch in archs:
        kernel_package_options.extend(
            [
                f"--include=pool/main/l/linux/linux-image*{arch}.deb",
                f"--include=pool/main/l/linux-signed-*/linux-image*{arch}.deb",
            ]
        )

    cmd = [
        "rsync",
        "-axPL",
        "-v",
        "--safe-links",
        "--prune-empty-dirs",
        "--del",
        "--delete-excluded",
        "--exclude=dists/**/*.diff/*",  # exclude pdiff dirs
        "--include=dists/",  # include each fragment of the path
        "--include=dists/*/",
        "--include=dists/*/*/",
        "--include=dists/*/*/binary-*/",
        *index_options,
        "--exclude=*linux-image-*dbg*",  # exclude lots of image variants
        "--exclude=*linux-image-*cloud*",
        "--exclude=*linux-image-*rt*",
        "--include=pool/",
        "--include=pool/main/",
        "--include=pool/main/l",
        "--include=pool/main/l/linux/",
        "--include=pool/main/l/linux-signed-*/",
        *kernel_package_options,
        "--exclude=*",  # exclude everything not explicitly included
        host,
        f"{cachebase}/",
    ]

    logger.info("rsync_mirror_data: rsync command: '%s'", "' '".join(cmd))

    output = subprocess.run(cmd, capture_output=True, check=False)

    if output.returncode > 0:
        logger.error(
            "rsync_mirror_data: error from rsync, returned %s", output.returncode
        )
        logger.error(output.stdout)
    else:
        logger.debug(output.stdout)

    return output.returncode == 0


##################################################################


def import_kernels(
    conn: Connection,
    base: Union[str, Path],
    release_arch_jobs: Iterable[Tuple[str, str]],
    kernel_filters: Dict[str, Tuple[str, str]],
    force: bool = False,
) -> bool:
    base = Path(base)
    success = []

    for release, arch in release_arch_jobs:
        rl = ResourceLocator(base / "dists" / release / "InRelease")
        packages_indexes = rl.packages("main", arch)

        logger.debug(
            "import_kernels: Looking for indexes: %s",
            " ".join(p["name"] for p in packages_indexes),
        )

        kl = KernelLocator(base / "dists" / release, packages_indexes)
        kernels = kl.find_kernels()

        kernels = kl.filter_kernels(kernels, kernel_filters.get(arch, None))

        logger.debug(
            "import_kernels: selected kernel packages: %s",
            " ".join(f"{k['package']}={k['filename']}" for k in kernels),
        )

        if not kernels:
            kinfo.importer.remove_kernel(conn, release, arch)
            continue

        for k in kernels:
            logger.info("import_kernels: processing %s", k["filename"])
            path = base / k["filename"]

            status = kinfo.application.imports.cmd_import_deb(
                conn, path, release, force
            )
            if status:
                logger.info("import_kernels: import completed for %s", k["filename"])
            else:
                logger.error("import_kernels: import error for %s", k["filename"])

            success.append(status)

    return all(success)


##################################################################


def import_contents(
    conn: Connection,
    base: Union[str, Path],
    releases: Iterable[str],
    architectures: Iterable[str],
) -> bool:

    logger.info("import_contents: ready to import for %s / %s", releases, architectures)

    success = kinfo.application.imports.cmd_import_contents(
        conn, base, releases, architectures
    )
    return success


##################################################################


def _fetcher(url: str, filename: Path) -> None:
    r = requests.get(url, timeout=timeout)
    r.raise_for_status()
    with open(filename, "wb") as fh:
        for chunk in r.iter_content(chunk_size=2048):
            fh.write(chunk)


def import_pciids(conn: Connection, url: str) -> bool:
    with tempfile.TemporaryDirectory() as tmpdirname:
        filename = Path(tmpdirname) / "pci.ids"
        logger.debug("import_pciids: '%s' -> '%s'", url, filename)
        _fetcher(url, filename)
        logger.debug("import_pciids: running importer")
        success = kinfo.application.imports.cmd_import_pciids(conn, filename)
    return success


def import_wiki_map(conn: Connection, url: str) -> bool:
    with tempfile.TemporaryDirectory() as tmpdirname:
        filename = Path(tmpdirname) / "wikimap"
        logger.debug("import_wiki_map: '%s' -> '%s'", url, filename)
        _fetcher(url, filename)
        logger.debug("import_wiki_map: running importer")
        success = kinfo.application.imports.cmd_import_wiki_map(conn, filename)
    return success


##################################################################


def database_setup(conn: Connection, force: bool) -> bool:
    logger.debug("database_setup: running importer")
    success = kinfo.application.imports.cmd_create_tables(conn, force)
    return success


#################################################################


def cmd_init(conn: Connection, force: bool) -> bool:
    return database_setup(conn, force)


def cmd_download(conf: kinfo.config.BulkImporterConfig) -> bool:
    return rsync_mirror_data(
        conf.rsynchost,
        conf.rsyncpath,
        conf.cachebase,
        conf.releases,
        conf.architectures,
    )


def cmd_import_all(
    conn: Connection,
    conf: kinfo.config.BulkImporterConfig,
    force: bool,
    release: Optional[str] = None,
    architecture: Optional[str] = None,
) -> bool:
    success = []

    releases = [release] if release else conf.releases
    architectures = [architecture] if architecture else conf.architectures
    status = import_kernels(
        conn,
        conf.cachebase,
        itertools.product(releases, architectures),
        conf.kernel_filters,
        force,
    )
    if not status:
        logger.error("import_all: import_kernels returned failure")
    success.append(status)

    status = import_contents(conn, conf.cachebase, conf.releases, conf.architectures)
    if not status:
        logger.error("import_all: import_contents returned failure")
    success.append(status)

    status = import_pciids(conn, conf.pciid_url)
    if not status:
        logger.error("import_all: import_pciids returned failure")
    success.append(status)

    status = import_wiki_map(conn, conf.wikimap_url)
    if not status:
        logger.error("import_all: import_wiki_map returned failure")
    success.append(status)

    # summary
    if not all(success):
        logger.error("Some actions failed: %s", success)

    return all(success)


#################################################################


def parse_args(argv: Optional[List[str]] = None) -> argparse.Namespace:
    # root argument parser
    parser = argparse.ArgumentParser(
        description="Run importer stages for kinfo db",
    )

    # create sub commands for each import action to be performed
    subparsers = parser.add_subparsers(dest="subcommand")
    subparsers.required = True

    # initialise database
    subparser = subparsers.add_parser("init", help="initialise the database")
    subparser.set_defaults(func=lambda conn, conf, a: cmd_init(conn, a.force))

    # download data from mirror
    subparser = subparsers.add_parser("sync", help="download data from mirror")
    subparser.set_defaults(func=lambda _, conf, __: cmd_download(conf))

    # import everything
    subparser = subparsers.add_parser("import", help="import the data files")
    subparser.add_argument(
        "--release",
        metavar="RELEASE",
        help="limit to specified release",
    )
    subparser.add_argument(
        "--arch",
        metavar="ARCH",
        help="limit to specified arch",
    )
    subparser.set_defaults(
        func=lambda conn, conf, a: cmd_import_all(
            conn, conf, a.force, a.release, a.arch
        )
    )

    # common options
    parser.add_argument(
        "--verbose",
        "-v",
        default=0,
        action="count",
        help="produce more verbose output" "(-v -v for more verbose)",
    )

    parser.add_argument(
        "--config",
        dest="config",
        metavar="FILE",
        default=None,
        help="the database connection configuration file",
    )

    parser.add_argument(
        "--force",
        action="store_true",
        help="force actions",
    )

    args = parser.parse_args(argv)
    return args


def main(argv: Optional[List[str]] = None) -> bool:
    args = parse_args(argv)
    kinfo.application.base.setup_logging(args.verbose)

    conf = kinfo.config.Config(args.config)
    db = kinfo.db.KinfoDB(conf)
    with db.connection() as conn:
        success = args.func(conn, conf.bulkimporter(), args)
        logger.debug("Finished process: %s", success)

    return not success
