# coding: utf-8
#
# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Match module data from a kernel with hardware using an SQL database.
"""
##############################################################################

# TODO
# match firmware to packages

##############################################################################

import argparse
import logging
from pathlib import Path
import textwrap

from typing import (
    Iterable,
    List,
    Optional,
    Union,
)

from psycopg import Connection

import kinfo.db
import kinfo.importer
import kinfo.search_cli
import kinfo.application.base


logger = logging.getLogger("kinfo-import")


##############################################################################


def cmd_import_deb(
    conn: Connection, deb: Union[str, Path], release: str, force: bool
) -> bool:
    status = kinfo.importer.import_deb(conn, deb, release, force)
    if status > 0:
        logger.info("import_deb: successful import of %s", deb)
    elif status == -1:
        logger.info("import_deb: skipped import of %s", deb)
    else:
        logger.error("import_deb: error importing %s", deb)

    return status >= -1


def cmd_import_contents(
    conn: Connection,
    tree: Union[Path, str],
    releases: Iterable[str],
    architectures: Iterable[str],
) -> bool:
    status = kinfo.importer.import_contents(
        conn, tree=tree, releases=releases, architectures=architectures
    )
    return status > 0


def cmd_import_pciids(conn: Connection, pciids: Union[str, Path]) -> bool:
    with open(pciids, 'rt', encoding="UTF-8") as pcimap:
        return kinfo.importer.import_pci_devices(conn, pcimap) > 0


def cmd_import_wiki_map(conn: Connection, wiki: Union[str, Path]) -> bool:
    with open(wiki, 'rt', encoding="UTF-8") as wikimap:
        return kinfo.importer.import_wiki_map(conn, wikimap) > 0


def cmd_create_tables(conn: Connection, force: bool) -> bool:
    # pylint: disable=unused-argument
    return kinfo.db.create_tables(conn)


##############################################################################


def main(argv: Optional[List[str]] = None) -> bool:
    """Import kernel module data into the database
    ---
    When importing information from a Debian kernel package, the package is
    unpacked and the details of the available modules are added to the database.
    The architecture and package version and uname -r are deduced from the
    filename of the package. The release must be specified.

    When importing information from the archive Contents.gz file, it is read
    and decompressed on the fly; a directory tree full of Contents files in
    the same layout as the Debian archive is required.

    Only one of the subcommands kernel, contents, pciids, wiki can be specified.
    See 'kinfo-importer {command} --help' for additional details and subcommand
    options.

    Examples:

    Import data from kernel package from sid:

        kinfo-importer kernel --release sid \\
            linux-image-3.16.0-4-amd64_3.16.7-ckt9-3~deb8u1_amd64.deb

    Import all Contents files for sid and experimental:

        kinfo-importer contents /srv/mirror/dists/ \\
                    --release sid --release experimental

    Import pciids data

        kinfo-importer pciids /usr/share/misc/pci.ids

    Import wikimap for module → wiki page mapping

        wget -U Kinfo -O modules.wikilinks \\
            http://wiki.debian.org/DeviceDatabase/ModuleToWikipageMap?action=raw
        kinfo-importer wiki modules.wikilinks

    """
    args = _importer_args(argv)
    kinfo.application.base.setup_logging(args.verbose)

    db = kinfo.db.KinfoDB(config=args.config)
    with db.connection() as conn:
        success = args.func(conn, args)
        logger.debug("Finished process: %s", success)

    return not success


def _importer_args(argv: Optional[List[str]] = None) -> argparse.Namespace:

    # Get the help information out of the docstring for the file
    (description, examples) = main.__doc__.split("---")  # type: ignore
    examples = textwrap.dedent(examples)

    parser = argparse.ArgumentParser(
        description=description,
        epilog=examples,
        formatter_class=kinfo.application.base.RawDefaultHelpFormatter,
    )

    # create sub commands for each import action to be performed
    subparsers = parser.add_subparsers(dest="subcommand")
    subparsers.required = True

    # import a kernel package
    subparser = subparsers.add_parser("kernel", help="import a kernel package")

    subparser.add_argument(
        "deb", metavar="linux-image.deb", help="kernel package file to import"
    )

    subparser.add_argument(
        "--release",
        required=True,
        action="store",
        help="release to record for the data",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_import_deb(conn, a.deb, a.release, a.force)
    )

    # import all the Contents files
    subparser = subparsers.add_parser("contents", help="import contents files")

    subparser.add_argument(
        "tree",
        metavar="DIRECTORY",
        help="Directory at the base of the Contents files tree",
    )

    subparser.add_argument(
        "--release",
        action="append",
        required=True,
        help="release to record for the data",
    )
    subparser.add_argument(
        "--arch",
        action="append",
        required=True,
        help="architectures to import",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_import_contents(conn, a.tree, a.release, a.arch)
    )

    # import the pci.ids file
    subparser = subparsers.add_parser("pciids", help="import pci.ids file")

    subparser.add_argument("pciids", metavar="pci.ids", help="Path to contents files")

    subparser.set_defaults(func=lambda conn, a: cmd_import_pciids(conn, a.pciids))

    # import the wiki module->page map
    subparser = subparsers.add_parser("wiki", help="import wiki module links file")

    subparser.add_argument(
        "wiki", metavar="modules.wikilinks", help="Path to wiki module map file"
    )

    subparser.set_defaults(func=lambda conn, a: cmd_import_wiki_map(conn, a.wiki))

    # get the database ready for import
    subparser = subparsers.add_parser("setup", help="set up the database")

    subparser.set_defaults(func=lambda conn, a: cmd_create_tables(conn, a.force))

    # common options
    parser.add_argument(
        "--verbose",
        "-v",
        default=0,
        action="count",
        help="produce more verbose output" "(-v -v for more verbose)",
    )

    parser.add_argument(
        "--config",
        dest="config",
        metavar="FILE",
        default=None,
        help="the database connection configuration file",
    )

    parser.add_argument(
        "--force",
        action="store_true",
        help="force actions",
    )

    args = parser.parse_args(argv)
    return args
