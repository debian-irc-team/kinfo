# coding: utf-8
#
# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Match module data from a kernel with hardware using an SQL database.
"""
##############################################################################

# TODO
# match firmware to packages

##############################################################################

import argparse
import logging
import textwrap

from typing import (
    Iterable,
    List,
    Optional,
)

from psycopg import Connection

import kinfo.db
import kinfo.importer
import kinfo.search_cli
import kinfo.application.base


logger = logging.getLogger("kinfo-search")


#############################################################################


def cmd_search_kernels(conn: Connection, release: str, arch: str) -> bool:
    logger.debug("search:kernels release=%s arch=%s", release, arch)
    return kinfo.search_cli.show_kernels_summary(conn, release, arch)


def cmd_search_modinfo(
    conn: Connection, release: str, arch: str, modules: Iterable[str]
) -> bool:
    logger.debug("search:modinfo release=%s arch=%s modules=%s", release, arch, modules)
    return kinfo.search_cli.show_modinfo(conn, release, arch, modules)


def cmd_search_pciid(
    conn: Connection,
    release: str,
    arch: str,
    pciids: Iterable[str],
    wildcards: bool,
) -> bool:
    logger.debug(
        "search:pciid release=%s arch=%s pciids=%s wild=%s",
        release,
        arch,
        pciids,
        wildcards,
    )
    return kinfo.search_cli.show_pciid(conn, release, arch, pciids, wildcards)


def cmd_search_lspci(
    conn: Connection,
    release: Optional[str] = None,
    arch: Optional[str] = None,
    modules: bool = False,
    firmware: bool = False,
) -> bool:
    logger.debug(
        "search:lspci release=%s arch=%s modules=%s firmware=%s",
        release,
        arch,
        modules,
        firmware,
    )
    return kinfo.search_cli.show_lspci(conn, release, arch, modules, firmware)


def cmd_search_firmware(
    conn: Connection, release: str, arch: str, firmware: Iterable[str]
) -> bool:
    logger.debug(
        "search:firmware release=%s arch=%s firmware=%s", release, arch, firmware
    )
    return kinfo.search_cli.show_firmware(conn, release, arch, firmware)


def cmd_search_kconfig(
    conn: Connection, release: str, arch: str, kconfig: Iterable[str]
) -> bool:
    logger.debug("search:kconfig release=%s arch=%s kconfig=%s", release, arch, kconfig)
    return kinfo.search_cli.show_kconfig(conn, release, arch, kconfig)


def cmd_search_pciname(conn: Connection, name: str) -> bool:
    logger.debug("search:pciname name=%s", name)
    return kinfo.search_cli.show_pciname_matches(conn, name)


#############################################################################


def main(argv: Optional[List[str]] = None) -> bool:
    """Search kernel module and hardware data from the database.
    ---
    Only one of the subcommands kernels, modinfo, pciid, firmware, kconfig,
    pciname can be specified. See 'kinfo-search {command} --help' for additional
    details and subcommand options.

    Examples:

    List indexed kernels:

        kinfo-search kernels

        kinfo-search kernels --release sid

    Show module info:

        kinfo-search modinfo --release sid iwlwifi

    Show module, firmware and firmware packages matching a PCI-Id:

        kinfo-search pciid --release sid 8086:4235

    Show which packages contain a given firmware file:

        kinfo-search firmware --release sid iwlwifi-6050-5.ucode

    Show the kernel config for a set of options (% is a wildcard):

        kinfo-search kconfig --release sid %CIFS%

    """
    args = _search_args(argv)
    kinfo.application.base.setup_logging(args.verbose)

    db = kinfo.db.KinfoDB(config=args.config)
    with db.connection() as conn:
        success = args.func(conn, args)

    return not success


def _search_args(argv: Optional[List[str]]) -> argparse.Namespace:

    # Get the help information out of the docstring for the file
    (description, examples) = main.__doc__.split("---")  # type: ignore
    examples = textwrap.dedent(examples)

    parser = argparse.ArgumentParser(
        description=description,
        epilog=examples,
        formatter_class=kinfo.application.base.RawDefaultHelpFormatter,
    )

    # create sub commands for each import action to be performed
    subparsers = parser.add_subparsers(dest="subcommand")
    subparsers.required = True

    # make some generic parsers for common arguments
    optional_release_parser = argparse.ArgumentParser(add_help=False)
    optional_release_parser.add_argument(
        "--release", action="store", metavar="RELEASE", help="release to search"
    )

    default_release_parser = argparse.ArgumentParser(add_help=False)
    default_release_parser.add_argument(
        "--release",
        action="store",
        default="sid",
        metavar="RELEASE",
        help="release to search (default: sid)",
    )

    default_arch_parser = argparse.ArgumentParser(add_help=False)
    default_arch_parser.add_argument(
        "--arch",
        "--architecture",
        action="store",
        default="amd64",
        metavar="ARCH",
        help="architecture to search",
    )

    # list indexed kernel packages
    subparser = subparsers.add_parser(
        "kernels",
        parents=[optional_release_parser, default_arch_parser],
        help="list indexed kernels for architecture",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_search_kernels(conn, a.release, a.arch)
    )

    # display info about a module
    subparser = subparsers.add_parser(
        "modinfo",
        parents=[default_release_parser, default_arch_parser],
        help="display information about a module",
    )
    subparser.add_argument(
        "modules",
        nargs="+",
        metavar="module",
        help="the module to display information about",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_search_modinfo(conn, a.release, a.arch, a.modules)
    )

    # search support given one or more PCI-Id
    subparser = subparsers.add_parser(
        "pciid",
        parents=[default_release_parser, default_arch_parser],
        help="search for support info for a PCI-Id",
    )
    subparser.add_argument(
        "pciids",
        nargs="+",
        metavar="PCIID",
        help="the PCI-Id of the device in the form xxxx:yyyy",
    )
    subparser.add_argument(
        "--wildcards",
        action="store_true",
        help="allow wildcard matches to devices",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_search_pciid(
            conn, a.release, a.arch, a.pciids, a.wildcards
        )
    )

    # search out all PCI hardware on the machine
    subparser = subparsers.add_parser(
        "lspci",
        parents=[default_release_parser, default_arch_parser],
        help="Look up current hardware",
    )
    subparser.add_argument(
        "--modules",
        action="store_true",
        help="include module information in output",
    )
    subparser.add_argument(
        "--firmware",
        action="store_true",
        help="include firmware information in output",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_search_lspci(
            conn, a.release, a.arch, a.modules, a.firmware
        )
    )

    # search for a firmware file
    subparser = subparsers.add_parser(
        "firmware",
        parents=[default_release_parser, default_arch_parser],
        help="search for packages including a piece of firmware",
    )
    subparser.add_argument(
        "firmware",
        nargs="+",
        metavar="filename",
        help="the filename of the firmware to locate",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_search_firmware(conn, a.release, a.arch, a.firmware)
    )

    # search kernel configuration
    subparser = subparsers.add_parser(
        "kconfig",
        parents=[default_release_parser, default_arch_parser],
        help="search for kernel config options",
    )
    subparser.add_argument(
        "kconfig",
        nargs="+",
        metavar="KCONFIG",
        help="the config item to locate (%% is wildcard)",
    )

    subparser.set_defaults(
        func=lambda conn, a: cmd_search_kconfig(conn, a.release, a.arch, a.kconfig)
    )

    # search for PCI hardware details on a partial device name
    subparser = subparsers.add_parser("pciname", help="search for device names")
    subparser.add_argument(
        "name",
        action="store",
        metavar="name",
        help="fragment of a device name (%% is wildcard)",
    )

    subparser.set_defaults(func=lambda conn, a: cmd_search_pciname(conn, a.name))

    # common options
    parser.add_argument(
        "--verbose",
        "-v",
        default=0,
        action="count",
        help="produce more verbose output" "(-v -v for more verbose)",
    )

    parser.add_argument(
        "--config",
        dest="config",
        metavar="FILE",
        default=None,
        help="the database connection configuration file",
    )

    args = parser.parse_args(argv)
    return args
