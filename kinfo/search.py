# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Search for information from the kernel info database
"""
##############################################################################

# TODO

##############################################################################

import logging
import re

from typing import Iterable, List, Literal, Optional, Tuple, TypedDict, Union, overload

from psycopg import Connection
from psycopg import Cursor


from ._utils import _normalise_module_name, _normalise_module_names

logger = logging.getLogger(__name__)

##############################################################################


def check_module_exists(
    c: Cursor,
    release: str,
    architecture: str,
    module: str,
    raises: bool = True,
) -> List[Tuple[int, str, int, str, str, str, str]]:
    """Check that a module is recorded in the database"""
    # normalise module name first
    module = _normalise_module_name(module)
    c.execute(
        r"""
                SELECT *
                FROM kinfodb.modules
                LEFT JOIN kinfodb.kernels ON kernels.id = modules.kernel_id
                WHERE
                    release = %(release)s AND
                    architecture = %(architecture)s AND
                    module = %(module)s
        """,
        {
            "release": release,
            "architecture": architecture,
            "module": module,
        }
    )

    modexists = c.fetchall()
    if raises and not modexists:
        raise ValueError("Module '%s' does not exist" % module)

    return modexists  # type: ignore


def check_kernel_exists(
    c: Cursor,
    release: str,
    architecture: str,
    raises: bool = True,
) -> List[Tuple[int, str, str, str, str]]:
    """ Check that a kernel is recorded in the db for (release, architecture) """
    c.execute(
        r"""
                SELECT *
                FROM kinfodb.kernels
                WHERE
                    release = %(release)s AND
                    architecture = %(architecture)s
        """,
        {"release": release, "architecture": architecture},
    )

    kernelexists = c.fetchall()
    if raises and not kernelexists:
        raise ValueError("Kernel '%s/%s' does not exist" % (release, architecture))

    return kernelexists  # type: ignore


##############################################################################

_pciid4re = re.compile(r"^\s*\[?([\da-f]{4}):([\da-f]{4})\]?\s*$", re.I)
_pciid8re = re.compile(r"^\s*\[?([\da-f]{8}):([\da-f]{8})\]?\s*$", re.I)
_part4re = re.compile(r"^[\da-f]{4}$", re.I)
_part8re = re.compile(r"^[\da-f]{8}$", re.I)


def pciid_check_format(pciid: str) -> bool:
    vendor, device = pciid_split(pciid)
    return vendor is not None and device is not None


def pciid_split(pciid: str) -> Tuple[Optional[str], Optional[str]]:
    vendor = None
    device = None
    for pciidre in [_pciid4re, _pciid8re]:
        m = pciidre.search(pciid)
        if m is not None:
            vendor = m.group(1).upper()
            device = m.group(2).upper()
            break
    return vendor, device


@overload
def clean_pciid_options(
    *,
    vendor: str = "0000",
    device: str = "0000",
    pciid: Literal[None] = None,
    wide: bool = True,
) -> Tuple[str, str]:
    pass


@overload
def clean_pciid_options(
    *,
    vendor: Literal[None] = None,
    device: Literal[None] = None,
    pciid: str = "0000:0000",
    wide: bool = True,
) -> Tuple[str, str]:
    pass


@overload
def clean_pciid_options(
    *,
    vendor: Optional[str] = None,
    device: Optional[str] = None,
    pciid: Optional[str] = None,
    wide: bool = True,
) -> Tuple[str, str]:
    pass


# FIXME: this typing isn't strong enough and allows multiple to be set


def clean_pciid_options(
    *,
    vendor: Optional[str] = None,
    device: Optional[str] = None,
    pciid: Optional[str] = None,
    wide: bool = True,
) -> Tuple[str, str]:
    # first check through the multitude of pciid/vendor/device options
    if pciid:
        # check exclusivity
        if vendor or device:
            raise ValueError("Must not specific vendor/device if pciid given")

        # try to split it up; pciid_split returns None on bad values
        vendor, device = pciid_split(pciid)
        if vendor is None or device is None:
            raise ValueError("Incorrect pciid given; could not be split")

    elif vendor and device:
        # check individual components are OK when given
        if not _part4re.match(vendor) and not _part8re.match(vendor):
            raise ValueError("Illegal vendor code given: %s" % vendor)
        if not _part4re.match(device) and not _part8re.match(device):
            raise ValueError("Illegal device code given: %s" % device)

    else:
        # Incorrect usage
        raise ValueError("Must specify either pciid or both vendor and device")

    if wide:
        if len(vendor) < 8:
            vendor = "0000%s" % vendor
        if len(device) < 8:
            device = "0000%s" % device

    return vendor, device


@overload
def pciid_lookup(
    conn: Connection,
    release: str,
    architecture: str,
    *,
    vendor: str,
    device: str,
    pciid: Literal[None] = None,
    wildcards: bool = True,
) -> List[str]:
    pass


@overload
def pciid_lookup(
    conn: Connection,
    release: str,
    architecture: str,
    *,
    vendor: Literal[None] = None,
    device: Literal[None] = None,
    pciid: str = "0000:0000",
    wildcards: bool = True,
) -> List[str]:
    pass


def pciid_lookup(
    conn: Connection,
    release: str,
    architecture: str,
    *,
    vendor: Optional[str] = None,
    device: Optional[str] = None,
    pciid: Optional[str] = None,
    wildcards: bool = True,
) -> List[str]:

    vendor, device = clean_pciid_options(
        vendor=vendor, device=device, pciid=pciid, wide=True
    )

    if wildcards:
        devices = [device.upper(), "FFFFFFFF"]
    else:
        devices = [device.upper()]

    logger.debug(
        "pciid_lookup: looking for VID %s DID in [%s]", vendor, ", ".join(devices)
    )

    c = conn.cursor()
    check_kernel_exists(c, release, architecture)
    c.execute(
        r"""
                SELECT DISTINCT module
                FROM kinfodb.pciids
                LEFT JOIN kinfodb.kernels ON kernels.id = pciids.kernel_id
                WHERE
                    release = %(release)s AND
                    architecture = %(architecture)s AND
                    vid = %(vid)s AND
                    did = ANY(%(dids)s)
        """,
        {
            "release": release,
            "architecture": architecture,
            "vid": vendor.upper(),
            "dids": devices,
        },
    )

    modules = [m[0] for m in c.fetchall()]
    logger.debug("pciid_lookup: found %d modules for device", len(modules))
    return modules


##############################################################################


def module_lookup(
    conn: Connection, release: str, architecture: str, module: str
) -> List[Tuple[str, str]]:
    c = conn.cursor()
    check_module_exists(c, release, architecture, module)
    module = _normalise_module_name(module)
    c.execute(
        r"""
                SELECT vid, did
                FROM kinfodb.pciids
                LEFT JOIN kinfodb.kernels ON kernels.id = pciids.kernel_id
                WHERE
                    release = %(release)s AND
                    architecture = %(architecture)s AND
                    module = %(module)s
        """,
        {"release": release, "architecture": architecture, "module": module},
    )

    devices = c.fetchall()
    logger.debug("module_lookup: found %d devices", len(devices))
    return devices  # type: ignore


##############################################################################


class KernelPackageVersions(TypedDict):
    release: str
    version: str
    uname: str


def kernel_lookup(
    conn: Connection, release: Optional[str], architecture: str
) -> List[KernelPackageVersions]:
    c = conn.cursor()
    if release:
        c.execute(
            r"""
                    SELECT DISTINCT release, version, uname
                    FROM kinfodb.kernels
                    WHERE
                        release = %(release)s AND
                        architecture = %(architecture)s
            """,
            {"release": release, "architecture": architecture},
        )
    else:
        c.execute(
            r"""
                    SELECT DISTINCT release, version, uname
                    FROM kinfodb.kernels
                    WHERE
                        architecture = %(architecture)s
            """,
            {"architecture": architecture},
        )
    kernels = [
        KernelPackageVersions(release=k[0], version=k[1], uname=k[2])
        for k in c.fetchall()
    ]
    logger.debug("kernel_lookup: found %d kernels", len(kernels))
    return kernels


##############################################################################


def kconfig_lookup(
    conn: Connection, release: str, architecture: str, config: str
) -> List[Tuple[str, str]]:
    if config.startswith("CONFIG_"):
        config = config[7:]
    config = config.upper()

    c = conn.cursor()
    check_kernel_exists(c, release, architecture)
    c.execute(
        r"""
                SELECT DISTINCT key, value
                FROM kinfodb.kconfig
                LEFT JOIN kinfodb.kernels ON kernels.id = kconfig.kernel_id
                WHERE
                    release = %(release)s AND
                    architecture = %(architecture)s AND
                    key ILIKE %(config)s
        """,
        {"release": release, "architecture": architecture, "config": config},
    )

    kconfigs = c.fetchall()
    logger.debug(
        "kconfig_lookup: found %d kconfig options containing %s", len(kconfigs), config
    )
    return kconfigs  # type: ignore


##############################################################################


def firmware_lookup(
    conn: Connection, release: str, architecture: str, module: str
) -> List[str]:
    """List firmware files that a module can load"""
    c = conn.cursor()
    check_module_exists(c, release, architecture, module)
    module = _normalise_module_name(module)
    c.execute(
        r"""
                SELECT DISTINCT filename
                FROM kinfodb.firmware
                LEFT JOIN kinfodb.kernels ON kernels.id = firmware.kernel_id
                WHERE
                    kernels.release = %(release)s AND
                    kernels.architecture = %(architecture)s AND
                    module = %(module)s
        """,
        {"release": release, "architecture": architecture, "module": module},
    )

    files = [f[0] for f in c.fetchall()]
    logger.debug("firmware_lookup: found %d firmware files for %s", len(files), module)
    return files


##############################################################################


def firmware_packages_lookup(
    conn: Connection,
    release: str,
    architecture: str,
    filenames: Union[str, Iterable[str]],
) -> List[str]:
    """ Find packages containing a firmware file """
    if not filenames:
        return []

    if isinstance(filenames, str):
        filenames = [filenames]
    c = conn.cursor()
    check_kernel_exists(c, release, architecture)
    c.execute(
        r"""
                SELECT DISTINCT package
                FROM kinfodb.firmware_packages
                WHERE
                    release = %(release)s AND
                    architecture = %(architecture)s AND
                    filename = ANY(%(filenames)s)
        """,
        {"release": release, "architecture": architecture, "filenames": list(filenames)},
    )

    packages = [p[0] for p in c.fetchall()]
    logger.debug(
        "firmware_packages_lookup: found %d packages containing %s",
        len(packages),
        filenames,
    )
    return packages


############################################################################


def fetch_pciid_match(
    conn: Connection,
    release: str,
    architecture: str,
    pciid: str,
    wildcards: bool = True,
) -> Tuple[List[str], List[str]]:
    """ Find modules firmware packages for a PCI Id """
    modules = pciid_lookup(
        conn, release, architecture, pciid=pciid, wildcards=wildcards
    )
    packages = []
    for module in modules:
        firmware_files = firmware_lookup(conn, release, architecture, module)
        if firmware_files:
            packages.extend(
                firmware_packages_lookup(conn, release, architecture, firmware_files)
            )
    return modules, sorted(set(packages))


###############################################################################


def wiki_match_lookup(conn: Connection, modules: Union[str, List[str]]) -> List[str]:
    """ Find any recorded wiki pages for the module """
    if isinstance(modules, str):
        modules = [modules]

    if not modules:
        return []

    modules = _normalise_module_names(modules)
    c = conn.cursor()
    c.execute(
        r"""
                SELECT wikipage
                FROM kinfodb.wiki_map
                WHERE
                    module = ANY(%(modules)s)
        """,
        {"modules": modules},
    )

    pages = [p[0] for p in c.fetchall()]
    logger.debug("wiki_match_lookup: found %d matching pages", len(pages))
    return pages


###############################################################################


def pciname_match_lookup(conn: Connection, name: str) -> List[str]:
    """ Find hardware matches to a case-insensitive fragment of a name """
    c = conn.cursor()
    c.execute(
        r"""
                SELECT vid, did, vendor, device
                FROM kinfodb.pci_devices
                WHERE
                    device ILIKE %(name)s
        """,
        {"name": name},
    )

    devices = c.fetchall()
    logger.debug("pciname_match_lookup: found %d matching devices", len(devices))
    return devices  # type: ignore


@overload
def pciid_name_lookup(
    conn: Connection,
    *,
    vendor: str = "0000",
    device: str = "0000",
    pciid: Literal[None] = None,
) -> Tuple[Optional[str], Optional[str]]:
    pass


@overload
def pciid_name_lookup(
    conn: Connection,
    *,
    vendor: Literal[None] = None,
    device: Literal[None] = None,
    pciid: str = "0000:0000",
) -> Tuple[Optional[str], Optional[str]]:
    pass


@overload
def pciid_name_lookup(
    conn: Connection,
    *,
    vendor: Optional[str] = None,
    device: Optional[str] = None,
    pciid: Optional[str] = None,
) -> Tuple[Optional[str], Optional[str]]:
    pass


def pciid_name_lookup(
    conn: Connection,
    *,
    vendor: Optional[str] = None,
    device: Optional[str] = None,
    pciid: Optional[str] = None,
) -> Tuple[Optional[str], Optional[str]]:
    """ Find hardware name for corresponding PCI Id"""

    vendor, device = clean_pciid_options(
        vendor=vendor, device=device, pciid=pciid, wide=True
    )

    logger.debug("pciid_name_lookup: looking for VID %s DID %s", vendor, device)

    c = conn.cursor()
    c.execute(
        r"""
                SELECT vid, did, vendor, device
                FROM kinfodb.pci_devices
                WHERE
                    vid = %(vid)s AND
                    did = %(did)s
                LIMIT 1
        """,
        {
            "vid": vendor.upper(),
            "did": device.upper(),
        },
    )
    match = c.fetchone()
    if not match:
        logger.debug(
            "pciid_name_lookup: found no matching device for VID %s DID %s",
            vendor,
            device,
        )
        return None, None

    vname, dname = match[2], match[3]
    logger.debug(
        "pciid_name_lookup: found matching devices for VID %s DID %s: '%s' from '%s'",
        vendor,
        device,
        vname,
        dname,
    )
    return vname, dname
