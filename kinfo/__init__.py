import importlib.metadata


# fetch the version from the pyproject.toml / installed wheel
try:
    __version__ = importlib.metadata.version(__package__ or __name__)
except importlib.metadata.PackageNotFoundError:
    __version__ = "0.0.0"
