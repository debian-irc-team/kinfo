#
# Kernel and firmware info database tool
#
# Configuration for database access
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

r"""
Config file support for kinfo

Expected format is as follows:
    [database]
    hostname: machine_name
    port:     database_port
    username: database_username
    password: database_password
    database: kinfo
"""

# See also:
# http://docs.python.org/library/configparser.html

import logging
import os
import os.path
from pathlib import Path
import configparser

from typing import Dict, List, Optional, Tuple, Union

FileLocationType = Union[str, Path]

logger = logging.getLogger(__name__)


class BulkImporterConfig:
    rsynchost: str
    rsyncpath: str
    cachebase: str
    releases: List[str]
    components: List[str]
    architectures: List[str]
    pciid_url: str
    wikimap_url: str
    kernel_filters: Dict[str, Tuple[str, str]]


class Config:
    config: configparser.RawConfigParser

    def __init__(
        self,
        filename: Optional[FileLocationType] = None,
        skipDefaultFiles: bool = False,
        confdict: Optional[Dict[str, str]] = None,
    ) -> None:
        self.filename = filename
        if not confdict:
            self.confdict = {}
        else:
            self.confdict = confdict
        self._ReadConfig(skipDefaultFiles)

    def _ReadConfig(self, skipDefaultFiles: bool = False) -> None:
        """
        Read in the database config for connecting to the UDD instance.

        Returns a dict with these keys:value pairs.
        """
        self.config = configparser.RawConfigParser()
        files: List[str] = []
        if not skipDefaultFiles:
            files.append("/etc/kinfo.conf")
            files.append(os.path.expanduser("~/.config/kinfo.conf"))
            files.append("kinfo.conf")

        if self.filename:
            if not os.path.isfile(self.filename):
                raise IOError("Configuration file not found '%s'." % self.filename)
            files.append(str(self.filename))

        if "KINFO_CONFIG" in os.environ and os.environ["KINFO_CONFIG"]:
            if not os.path.isfile(os.environ["KINFO_CONFIG"]):
                raise IOError(
                    "Configuration file not found: '%s'." % os.environ["KINFO_CONFIG"]
                )
            files.append(os.environ["KINFO_CONFIG"])

        logger.debug("Config: searching files for config: %s", files)

        files = self.config.read(files)

        if not files and not self.confdict:
            raise ValueError(
                "No valid configuration was found to connect to the database."
            )

        no_database_conf = False
        if not self.config.has_section("database"):
            self.config.add_section("database")
            no_database_conf = True

        for opt in self.confdict.keys():
            self.config.set("database", opt, self.confdict[opt])
            if opt == "database":
                no_database_conf = False

        if no_database_conf:
            logger.error("Config: no database configuration present.")

    def db(self) -> Dict[str, Optional[str]]:
        """
        Read in the database config for connecting to the UDD instance.

        Returns a dict with these keys:value pairs.
        """
        conf = {
            "hostname": self.get("database", "hostname", "localhost"),
            "port": self.get("database", "port", "5432"),
            "username": self.get("database", "username", None),
            "password": self.get("database", "password", None),
            "database": self.get("database", "database", "kinfo"),
        }
        return conf

    def bulkimporter(self) -> BulkImporterConfig:
        conf = BulkImporterConfig()
        s = "bulk-importer"
        for key in ["rsynchost", "rsyncpath", "cachebase", "pciid_url", "wikimap_url"]:
            val = self.get(s, key)
            if val is not None:
                val = val.strip()
            setattr(conf, key, val)
        for key in ["releases", "components", "architectures"]:
            val = self.get(s, key)
            vals = (val if val is not None else "").splitlines()
            val_list = [v for v in vals if v]
            setattr(conf, key, val_list)

        conf.kernel_filters = self._unpack_kernel_filters(self.get(s, "kernel_filters"))
        return conf

    @staticmethod
    def _unpack_kernel_filters(val: Optional[str]) -> Dict[str, Tuple[str, str]]:
        kernel_filters = {}
        if val:
            try:
                for v in val.strip().splitlines():
                    a, pos, neg = [p.strip() for p in v.split(",")]
                    kernel_filters[a] = (pos, neg)
            except (IndexError, ValueError) as e:
                logger.error(
                    "config:bulkimporter: error in config item kernel_filters: %s", e
                )
        else:
            logger.error("config:bulkimporter: no kernel_filters found in config")

        return kernel_filters

    def get(
        self, section: str, value: str, default: Optional[str] = None
    ) -> Optional[str]:
        try:
            return self.config.get(section, value)
        except (KeyError, configparser.NoSectionError, configparser.NoOptionError):
            return default
