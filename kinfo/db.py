# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Database management for kernel info kinfo
"""
##############################################################################

# TODO

##############################################################################

from contextlib import contextmanager
import logging
from pathlib import Path

from typing import (
    Any,
    Iterator,
    Optional,
    Union,
)

import psycopg

from .config import Config


logger = logging.getLogger(__name__)

setup_sql_filename = Path(__file__).parent / "setup.sql"


##############################################################################


class KinfoDB:
    def __init__(
        self,
        config: Optional[Union[str, Path, Config]] = None,
        db: Optional[psycopg.Connection] = None,
    ):
        self.psql: Optional[psycopg.Connection] = None

        if isinstance(config, (str, Path)) or (config is None and db is None):
            config = Config(config)
        self.config = config

        # Find the database connection, either provided or by connecting
        if not db:
            self.connect()
        else:
            self.psql = db

    def __del__(self) -> None:
        self._disconnect()

    def connect(self) -> psycopg.Connection:
        if self.psql:
            logger.debug("connect: using existing database connection")
            return self.psql

        if self.config is None:
            raise ValueError("No configuration supplied")

        arg_mapping = {
            "database": "dbname",
            "hostname": "host",
            "port": "port",
            "password": "password",
            "username": "user",
        }
        kwargs: dict[str, Any] = {}
        c = self.config.db()
        for i in c.keys():
            if c[i] is not None:
                kwargs[arg_mapping[i]] = c[i]

        # make the connection
        logger.debug("connect: creating new database connection")
        psql = psycopg.connect(**kwargs, client_encoding="UTF8")
        self.psql = psql
        return psql

    def _disconnect(self) -> None:
        if self.psql:
            logger.debug("connect: disconnecting from database")
            self.psql.close()

    @contextmanager
    def connection(self) -> Iterator[psycopg.Connection]:
        try:
            yield self.psql if self.psql else self.connect()
        finally:
            self._disconnect()


def create_tables(conn: psycopg.Connection) -> bool:
    """ create the tables in the database """
    c = conn.cursor()

    with open(setup_sql_filename, "r", encoding="UTF-8") as sql:
        c.execute(sql.read())
        conn.commit()
    return True
