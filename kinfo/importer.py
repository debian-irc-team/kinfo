# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Import module data from a kernel into an SQL database.
"""
##############################################################################

# TODO

##############################################################################

import collections
import logging
from pathlib import Path
import re
import subprocess
import tempfile

from typing import (
    Dict,
    Iterable,
    List,
    Optional,
    Tuple,
    Union,
)

from psycopg import Connection, Cursor

from pydebcontents import ContentsFile, ContentsDict


from ._utils import _normalise_module_name, _normalise_module_names


logger = logging.getLogger(__name__)


##############################################################################

__modinfo_linere = re.compile(r"(?P<key>.*?)[:=]\s*(?P<value>.*)")

# PCI Device matching info: for more information see
# http://anonscm.debian.org/cgit/collab-maint/isenkram.git/tree/modalias-format.txt
#
# pci:v00008086d00002770sv00001028sd000001ADbc06sc00i00
#
# This represent these values:
#
# v   00008086  (vendor)
# d   00002770  (device)
# sv  00001028  (subvendor)
# sd  000001AD  (subdevice)
# bc  06        (bus class)
# sc  00        (bus subclass)
# i   00        (interface)

PciAlias = collections.namedtuple("PciAlias", ["v", "d", "sv", "sd", "bc", "sc", "i"])

__pcialias_re = re.compile(
    r"""
    pci:                    # modalias device type
    v(?P<v>(.{8}|\*))       # vendor id
    d(?P<d>(.{8}|\*))       # device id
    sv(?P<sv>(.{8}|\*))
    sd(?P<sd>(.{8}|\*))
    bc(?P<bc>(.{2}|\*))
    sc(?P<sc>(.{2}|\*))
    i(?P<i>(.{2}|\*))
""",
    re.VERBOSE,
)

modinfo_tool = "/sbin/modinfo"


KernelPackage = collections.namedtuple(
    "KernelPackage",
    [
        "filename",
        "uname",
        "version",
        "architecture",
    ],
)

__kernel_packagere = re.compile(
    r"""
    .*                      # directory structure
    linux-image-            # package name stem
    (?P<uname>.*)_          # package name includes `uname -r`
    (?P<version>.*)_        # package version
    (?P<architecture>.*)    # package architecture
    \.deb                   # file extension
""",
    re.VERBOSE,
)

##########################################################################


def kernel_package(filename: Union[Path, str]) -> KernelPackage:
    """ figure out kernel details from the filename """
    m = __kernel_packagere.match(str(filename))
    if m:
        parts = m.groupdict()
        return KernelPackage(filename=filename, **parts)

    raise ValueError("Package filename doesn't look right: %s" % filename)


##########################################################################


def find_modules(tree: Union[str, Path]) -> List[Path]:
    """find the available kernel modules within the specified kernel tree

    Finds all the kernel modules files that are available within the specified
    tree.

    returns:
        list of absolute filenames as pathlib.Path objects
    """
    modulere = re.compile(r".*\.k*o(\.[gx]z)?$")

    modules = []
    for pth in Path(tree).glob("**/*"):
        if pth.is_file() and modulere.match(str(pth)):
            modules.append(pth)
    return sorted(modules)


def modinfo(filename: Union[str, Path]) -> Dict[str, List[str]]:
    """obtain information about a kernel module file

    returns: dict of lists for each data type returned
        {
            'alias' : [ 'pci:...', 'pci:...', ... ],
            'parm' : [ 'debug:...', ... ],
        }
    """
    filename = Path(filename)
    info_b = subprocess.check_output([modinfo_tool, "-0", str(filename)])
    try:
        info = info_b.decode("UTF-8")
    except UnicodeError:  # pragma: no cover
        # crappy fallback for some modules?
        info = info_b.decode("ISO8859-1")

    module_name = filename.stem
    return _parse_modinfo(info, module_name)


def _parse_modinfo(info: str, module_name: str) -> Dict[str, List[str]]:
    """convert the modinfo output into a useful format

    returns: format as described by `modinfo` function
    """
    data: Dict[str, List[str]] = collections.defaultdict(list)
    for line in info.split("\0"):
        m = __modinfo_linere.match(line)
        if m:
            parts = m.groupdict()
            data[parts["key"]].append(parts["value"])

    data["name"] = _normalise_module_names(data.get("name", [module_name]))
    return data


def pciid(info: str) -> Optional[PciAlias]:
    """construct a PciAlias object from modinfo output

    note that PCI-Id matching is more sophisticated than the simple vendor
    and device Id that is extracted here, but this is often good enough.

    returns:
        list of PciAlias (named tuples)
    """
    m = __pcialias_re.match(info)
    if m:
        parts = m.groupdict()
        return PciAlias(**parts)
    return None


##########################################################################


def import_all_modules(
    c: Cursor, kernel_id: int, tree: Union[Path, str], limit: Optional[int] = None
) -> List[Path]:
    """index all modules for the specified kernel

    Walk the file system, find all modules and index their metadata.

    c:         sql cursor
    kernel_id: primary id of the kernel in the database
    tree:      filesystem location of unpacked kernel pacakage to be imported
    """
    logger.info("import_all_modules: processing all modules for kernel %d", kernel_id)
    modules = find_modules(Path(tree))
    if limit:
        modules = modules[:limit]
    import_modules(c, kernel_id, modules)
    return modules


def _register_module(cursor: Cursor, kernel_id: int, module: str) -> None:
    """add module entry to database"""
    cursor.execute(
        """
        INSERT INTO kinfodb.modules (kernel_id, module)
            VALUES (%s, %s)
        """,
        (kernel_id, module),
    )


def _register_pci_alias(
    cursor: Cursor,
    kernel_id: int,
    module: str,
    pci: PciAlias,
) -> None:
    """add pciid entry to database"""
    vid8 = pci.v.upper()
    did8 = pci.d.upper()
    if vid8 == "*":
        vid8 = "FFFFFFFF"
    if did8 == "*":
        did8 = "FFFFFFFF"

    if len(vid8) != 8 or len(did8) != 8:
        # don't like this value; log and keep going though
        logger.error(
            "import_all_modules: PCI Ids should be 8 chars for both vid and did. Got: %s:%s",
            vid8,
            did8,
        )
        return

    cursor.execute(
        """
        INSERT INTO kinfodb.pciids (kernel_id, module, vid, did)
            VALUES (%s, %s, %s, %s)
        """,
        (kernel_id, module, vid8, did8),
    )


def _register_firmware(
    cursor: Cursor, kernel_id: int, module: str, firmware: str
) -> None:
    """add firmware entry to database"""
    cursor.execute(
        """
        INSERT INTO kinfodb.firmware (kernel_id, module, filename)
            VALUES (%s, %s, %s)
        """,
        (kernel_id, module, firmware),
    )


def import_modules(
    cursor: Cursor, kernel_id: int, modules: Iterable[Union[str, Path]]
) -> None:
    """index the specified modules

    Index the metadata of the modules into the database.

    cursor:    sql cursor
    kernel_id: primary id of the kernel in the database
    modules:   list of absolute paths to the modules
    """
    logger.info("import_modules: Loading modules for kernel %d", kernel_id)

    # process the list of modules
    count = dict.fromkeys(["total", "pci", "firmware"], 0)
    for m in modules:
        count["total"] += 1
        info = modinfo(m)
        _register_module(cursor, kernel_id, info["name"][0])
        if "alias" in info:
            for alias in info["alias"]:
                pci = pciid(alias)
                if pci:
                    count["pci"] += 1
                    _register_pci_alias(cursor, kernel_id, info["name"][0], pci)
        if "firmware" in info:
            for firmware in info["firmware"]:
                count["firmware"] += 1
                _register_firmware(cursor, kernel_id, info["name"][0], firmware)

    logger.info(
        "import_modules: Imported data for %d modules (PCI: %d, Firmware: %d)",
        count["total"],
        count["pci"],
        count["firmware"],
    )


###########################################################################
###########################################################################

def find_config(tree: Union[str, Path], uname: str) -> Path:
    """generate the file path for the kernel config

    Within the specified kernel tree, calculate the normal
    kernel config filename and path, noting that this
    seems to be a fragile across kernel archs and flavours.

    It is not verified that the file exists.

    returns:
        Path object with the file path and name of config file.
        If the 'tree' is absolute, this will be an absolute path.
    """
    uname = uname.removesuffix("-unsigned")
    return Path(tree) / "boot" / ("config-%s" % uname)


def load_config_file(filename: Union[Path, str]) -> List[Tuple[str, str]]:
    """load a kernel config file

    returns:
        list of tuples of config items
    """
    config_set_re = re.compile(r"^CONFIG_(.*?)\s*=\s*(.*)")
    config_unset_re = re.compile(r"^#\s*CONFIG_(.*?) is not set")

    conf = []
    with open(filename, "r", encoding="UTF-8") as conffh:
        for line in conffh:
            m = config_set_re.match(line)
            if m:
                conf.append((m.group(1), m.group(2)))
                continue
            m = config_unset_re.match(line)
            if m:
                conf.append((m.group(1), "n"))
                continue
    return conf


##############################################################################


def _register_kconfig(cursor: Cursor, kernel_id: int, key: str, value: str) -> None:
    """add kernel config entry to database"""
    cursor.execute(
        """
        INSERT INTO kinfodb.kconfig (kernel_id, key, value)
            VALUES (%s, %s, %s)
        """,
        (kernel_id, key, value),
    )


def import_kconfig(
    cursor: Cursor, kernel_id: int, uname: str, tree: Union[Path, str]
) -> None:
    """index the configuration for the specified kernel

    Parse the config file that was used to compile the kernel
    """
    logger.info("import_kconfig: processing config for kernel %d", kernel_id)

    # find config file
    configfile = find_config(tree, uname)
    logger.debug("import_kconfig: kernel config file %s", configfile)
    config = load_config_file(configfile)

    # process all config
    count = 0
    for c in config:
        count += 1
        _register_kconfig(cursor, kernel_id, c[0], c[1])

    logger.info("import_kconfig: imported data for %d config items", count)


##############################################################################
##############################################################################


def kernel_indexed(
    cursor: Cursor, version: str, uname: str, release: str, architecture: str
) -> int:
    """Check if the kernel package is already indexed

    returns:
        kernel id if kernel is already indexed; -1 if not
    """
    cursor.execute(
        """
        SELECT id from kinfodb.kernels
        WHERE version=%s AND
            uname=%s AND
            release=%s AND
            architecture=%s
        ORDER BY id DESC
        LIMIT 1
        """,
        (version, uname, release, architecture),
    )
    kernel_id = cursor.fetchone()
    if not kernel_id:
        return -1

    return int(kernel_id[0])


def _remove_old_kernels(cursor: Cursor, release: str, architecture: str) -> None:
    """remove old kernel versions from the kernels table

    the data associated with these kernels will be removed via CASCADE
    within the database
    """
    cursor.execute(
        """
        DELETE FROM kinfodb.kernels
        WHERE release=%s AND
            architecture=%s
        """,
        (release, architecture),
    )


def _remove_kernels(cursor: Cursor, kernel_ids: Union[Iterable[int], int]) -> None:
    """remove a kernel version from the kernels table

    the data associated with these kernels will be removed later
    """
    if isinstance(kernel_ids, int):
        kernel_ids = [kernel_ids]

    cursor.execute(
        """
        DELETE FROM kinfodb.kernels
        WHERE id = ANY(%(ids)s)
        """,
        {"ids": list(kernel_ids)},
    )


def _get_new_kernel_id(
    cursor: Cursor,
    version: str,
    uname: str,
    release: str,
    architecture: str,
) -> int:
    """ record the new kernel version and return a row id """
    cursor.execute(
        """
        INSERT INTO kinfodb.kernels (version, uname, release, architecture)
                VALUES (%s, %s, %s, %s)
        """,
        (version, uname, release, architecture),
    )
    # cursor.execute("SELECT last_insert_rowid()") #FIXME: sqlite specific?

    kernel_id = kernel_indexed(cursor, version, uname, release, architecture)

    logger.debug("import_kernel: kernel record %d created", kernel_id)
    return kernel_id


def import_kernel(
    conn: Connection,
    version: str,
    uname: str,
    release: str,
    architecture: str,
    tree: str,
    force: bool = False,
    limit: Optional[int] = None,
) -> int:
    """import data for an unpacked kernel package

    The kernel module metadata and the kernel config are imported
    into the database in a transaction.
        conn:     db connection object
        version:
    """
    logger.info(
        "import_kernel: processing kernel %s (%s) for %s/%s",
        uname,
        version,
        release,
        architecture,
    )
    c = conn.cursor()

    oldidx = kernel_indexed(c, version, uname, release, architecture)
    if oldidx > -1:
        logger.info(
            "import_kernel: kernel %s/%s/%s/%s already indexed as %s",
            version,
            uname,
            release,
            architecture,
            oldidx,
        )
        if not force:
            logger.info("import_kernel: skipping reimport")
            return -1

    _remove_old_kernels(c, release, architecture)
    kernel_id = _get_new_kernel_id(c, version, uname, release, architecture)

    treepth = Path(tree)
    # transparently handle merged-/usr and unmerged-/usr kernel layouts
    modulepth = treepth / "usr" / "lib" / "modules" / uname
    if not modulepth.exists():
        modulepth = treepth / "lib" / "modules" / uname

    import_all_modules(c, kernel_id, modulepth, limit)
    import_kconfig(c, kernel_id, uname, tree)

    conn.commit()
    logger.info("import_kernel: finished importing kernel record %d", kernel_id)
    return kernel_id


def remove_kernel(
    conn: Connection,
    release: str,
    architecture: str,
) -> None:
    """remove an obsoleted kernel from the database"""
    logger.info(
        "remove_kernel: removing kernel for %s/%s",
        release,
        architecture,
    )
    c = conn.cursor()
    _remove_old_kernels(c, release, architecture)


def import_deb(
    conn: Connection,
    deb: Union[Path, str],
    release: str,
    force: bool = False,
    limit: Optional[int] = None,
) -> int:
    """unpack a kernel binary package and then import it

    returns:
        kernel_id from the database if imported, else None
    """
    kp = kernel_package(deb)
    with tempfile.TemporaryDirectory(prefix="tmp-dpkg-") as tmpdir:
        logger.debug("import_deb: extracting %s to %s", deb, tmpdir)
        subprocess.run(
            ["dpkg", "-x", deb, tmpdir],
            check=True,
            capture_output=True,
        )
        return import_kernel(
            conn,
            kp.version,
            kp.uname,
            release,
            kp.architecture,
            tree=tmpdir,
            force=force,
            limit=limit,
        )


##############################################################################
##############################################################################


def _register_firmware_file(
    cursor: Cursor, release: str, architecture: str, package: str, filename: str
) -> None:
    """ add an entry for a firmware file to the database """
    cursor.execute(
        """
        INSERT INTO kinfodb.firmware_packages
            (release, architecture, package, filename)
            VALUES (%s, %s, %s, %s)
        """,
        (release, architecture, package, filename),
    )


def import_firmware_packages(
    conn: Connection,
    release: str,
    architecture: str,
    matches: ContentsDict,
) -> int:
    """ import pacakge information for location of the firmware """
    cursor = conn.cursor()
    cursor.execute(
        """
        DELETE FROM kinfodb.firmware_packages
        WHERE release=%s AND architecture=%s
        """,
        (release, architecture),
    )
    count = 0
    for package_info, paths in matches.items():
        for path in paths:
            path = path.removeprefix("usr/")
            filename = path.removeprefix("lib/firmware/")
            package = package_info.split("/")[-1]
            _register_firmware_file(cursor, release, architecture, package, filename)
            count += 1
    conn.commit()
    return count


def import_contents(
    conn: Connection,
    releases: Iterable[str],
    architectures: Optional[Iterable[str]] = None,
    components: Optional[Iterable[str]] = None,
    tree: Union[str, Path] = ".",
) -> int:
    """import firmware filenames and packages"""
    architectures = architectures or ["i386", "amd64"]
    components = components or ["main", "contrib", "non-free", "non-free-firmware"]
    count = 0
    for release in releases:
        for architecture in architectures:
            search_archs = [architecture, "all"]
            logger.info(
                "import_contents: processing Contents file for %s/%s %s",
                release,
                search_archs,
                components,
            )

            contents = ContentsFile(
                tree, release, search_archs, components
            )

            contents.maxhits = 1000000
            regexp = "^(usr/)?lib/firmware"
            matches = contents.search(regexp)
            logger.debug("import_contents: found %d firmware packages", len(matches))
            c = import_firmware_packages(conn, release, architecture, matches)
            count += c
    return count


##############################################################################
##############################################################################


def _register_pci_device(
    cursor: Cursor, vid: str, did: str, vendor: str, device: str
) -> None:
    """ add a PCI device record to the database """
    if len(vid) != 8 or len(did) != 8:
        logger.error(
            "import_pci_devices: PCI Ids should be 8 chars for both vendor and device. Got: %s:%s",
            vid,
            did,
        )
        return

    cursor.execute(
        """
        INSERT INTO kinfodb.pci_devices
            (vid, did, vendor, device)
            VALUES (%s, %s, %s, %s)
        """,
        (vid.upper(), did.upper(), vendor, device),
    )


def import_pci_devices(
    conn: Connection,
    pciidmap: Iterable[str],
) -> int:
    """import data in format of pci.ids file into the database

    any file like object is accepted
    """
    logger.debug("import_pci_devices: importing data")
    cursor = conn.cursor()
    cursor.execute(
        """
        DELETE FROM kinfodb.pci_devices
        """
    )

    vendor_line_re = re.compile(
        r"""
        ^(?P<vid>[0-9a-f]{4})\s+    # vendor id
        (?P<vendor>.+)              # vendor name
    """,
        re.I | re.VERBOSE,
    )
    device_line_re = re.compile(
        r"""
        ^\t(?P<did>[0-9a-f]{4})\s+  # device id
        (?P<device>.+)              # device name
    """,
        re.I | re.VERBOSE,
    )
    subdevice_line_re = re.compile(
        r"""
        ^\t\t(?P<did>[0-9a-f]{4})   # device id
        \s(?P<sdid>[0-9a-f]{4})\s+  # subdevice id
        (?P<device>.+)              # device name
    """,
        re.I | re.VERBOSE,
    )

    count = 0
    for line in pciidmap:
        if len(line) < 4 or line[0] == "#":
            continue
        matches = vendor_line_re.match(line)
        if matches:
            vid = matches.group("vid")
            vid8 = "0000%s" % vid
            vendor = matches.group("vendor").strip()
        else:
            matches = device_line_re.match(line)
            if matches and not subdevice_line_re.match(line):
                did = matches.group("did")
                did8 = "0000%s" % did
                device = matches.group("device")

                _register_pci_device(cursor, vid8, did8, vendor, device)
                count += 1

    logger.info("import_pci_devices: imported %d PCI device entries", count)
    conn.commit()
    return count


##############################################################################
##############################################################################


def _register_wiki_link(cursor: Cursor, module: str, wikipage: str) -> None:
    """ add a wiki page record to the database """
    cursor.execute(
        """
        INSERT INTO kinfodb.wiki_map
            (module, wikipage)
            VALUES (%s, %s)
        """,
        (module, wikipage),
    )


def import_wiki_map(conn: Connection, wikimap: Iterable[str]) -> int:
    """import data from wiki module mapping file into the database

    any file like object is accepted
    """
    logger.debug("import_wiki_map: importing data")
    cursor = conn.cursor()
    cursor.execute(
        """
        DELETE FROM kinfodb.wiki_map
        """
    )

    module_line_re = re.compile(
        r"""
        ^\s*\*\s+(?P<module>.+?)\s+     # module name
        (?P<pages>\[\[.+\]\])           # wiki page names
    """,
        re.VERBOSE,
    )
    pages_re = re.compile(r"\[\[([^]]+)\]\]")

    count = 0
    for line in wikimap:
        matches = module_line_re.match(line)
        if matches:
            module = _normalise_module_name(matches.group("module"))
            pages = matches.group("pages")

            for page in re.split(r"[\s,]", pages):
                pagematches = pages_re.match(page)
                if pagematches:
                    _register_wiki_link(cursor, module, pagematches.group(1))
                    count += 1
                else:  # pragma: no cover
                    logger.warning("Couldn't handle line: '%s'", line)

    logger.info("import_wiki_map: imported %d wiki map entries", count)
    conn.commit()
    return count
