# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

from contextlib import contextmanager

from typing import Any, Generator, Iterable, Optional

import pytest


@pytest.mark.usefixtures("stdout")
class _StdOutTestMixin:

    # from the stdout fixture
    capsys: Any

    @contextmanager
    def assertInStdOut(
        self,
        needles: Optional[Iterable[str]] = None,
        absent_needles: Optional[Iterable[str]] = None,
        err_needles: Optional[Iterable[str]] = None,
        err_absent_needles: Optional[Iterable[str]] = None,
        msg: Optional[str] = None,
    ) -> Generator[None, None, None]:
        # Generate a test failure if useful values of 'needles' not given
        if not any([needles, absent_needles, err_needles, err_absent_needles]):
            raise ValueError("No search terms provided for stdout")  # pragma: no cover

        captured = self.capsys.readouterr()
        try:
            yield

        finally:
            captured = self.capsys.readouterr()
            if needles:
                for needle in needles:
                    assert needle in captured.out, msg  # type: ignore
            if absent_needles:  # pragma: no cover
                for needle in absent_needles:
                    assert needle not in captured.out, msg  # type: ignore
            if err_needles:  # pragma: no cover
                for needle in err_needles:
                    assert needle in captured.err, msg  # type: ignore
            if err_absent_needles:  # pragma: no cover
                for needle in err_absent_needles:
                    assert needle not in captured.err, msg  # type: ignore
