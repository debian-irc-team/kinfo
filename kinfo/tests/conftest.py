# Kernel module/firmware info database
#
# Database fixture for test suite
#
###
#
# Copyright (c) 2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Database fixture for tests """

import datetime
from pathlib import Path
import re
import subprocess

from typing import Any, Iterable, List, Optional, TypedDict, Union

import bs4
import requests
import pytest
from pytest_postgresql import factories
import psycopg

from kinfo.db import KinfoDB
from kinfo import importer


kernel_index_page = "https://deb.debian.org/debian/pool/main/l/linux-signed-amd64/"


max_age = 24 * 3600  # one day
timeout = 180        # s

##########################################################################
# test files: database schema that is within the module itself


def find_module_data(filename: str) -> Path:
    """ find a test file that is located within the test suite """
    return (Path(__file__).parent / filename).resolve()


##########################################################################
# test suite database setup

setup_file = "../setup.sql"


class SampleModInfoType(TypedDict):
    module: str
    firmware: List[str]
    pciid: List[str]


sample_data_module: List[SampleModInfoType] = [
    {
        "module": "bnx2",
        "firmware": [
            "bnx2-rv2p-09ax-6.0.17.fw",
            "bnx2-rv2p-09-6.0.17.fw",
            "bnx2-mips-09-6.2.1b.fw",
        ],
        "pciid": [
            "pci:v000014E4d0000163Csv*sd*bc*sc*i*",
            "pci:v000014E4d0000163Bsv*sd*bc*sc*i*",
        ],
    },
    {
        "module": "bnx2nofw",
        "firmware": [],
        "pciid": [
            "pci:v000014E4d0000165Csv*sd*bc*sc*i*",
            "pci:v000014E4d0000165Bsv*sd*bc*sc*i*",
        ],
    },
    {
        "module": "bnx2nodev",
        "firmware": [],
        "pciid": [],
    },
    {
        "module": "snd_hda_intel",
        "firmware": [],
        "pciid": [
            "pci:v00008086d*sv*sd*bc04sc03i00*",
            "pci:v00008086d00003A6Esv*sd*bc*sc*i*",
        ],
    },
    {
        "module": "iwlwifi",
        "firmware": [
            "iwlwifi-100-5.ucode",
        ],
        "pciid": [
            "pci:v00008086d00007E80sv*sd*bc*sc*i*",
            "pci:v00008086d00007AF0sv*sd00000510bc*sc*i*",
        ],
    },
]


sample_data_firmware = {
    "firmware-bnx2": [
        "bnx2-rv2p-09ax-6.0.17.fw",
        "bnx2-rv2p-09-6.0.17.fw",
        "bnx2-mips-09-6.2.1b.fw",
    ],
    "firmware-iwlwifi": [
        "iwlwifi-100-5.ucode",
    ],
}


# Real data would come from
# http://wiki.debian.org/DeviceDatabase/ModuleToWikipageMap?action=raw
sample_data_wikimap = [
    # (module, page)
    ("bnx2", "wiki-bnx2-page"),
    ("iwlwifi", "iwlwifi"),
    ("foo", "bar"),
]

sample_data_kconfig = [
    ("SCSI_BNX2X_FCOE", "m"),
    ("BNX2", "m"),
    ("BNX2X", "m"),
    ("BNX2X_SRIOV", "y"),
]


sample_data_pciids = [
    (
        "000014E4",
        "0000163C",
        "Broadcom Corporation",
        "NetXtreme II BCM5716S Gigabit Ethernet",
    ),
    (
        "000014E4",
        "0000163D",
        "Broadcom Corporation",
        "NetXtreme II BCM57811 10-Gigabit Ethernet",
    ),
    (
        "000014E4",
        "0000163E",
        "Broadcom Corporation",
        "NetXtreme II BCM57811 10-Gigabit Ethernet Multi Function",
    ),
    (
        "000014E4",
        "0000163F",
        "Broadcom Corporation",
        "NetXtreme II BCM57811 10-Gigabit Ethernet Virtual Function",
    ),
    (
        "00008086",
        "00000089",
        "Intel Corporation",
        "Centrino Advanced-N + WiMAX 6250 [Kilmer Peak]",
    ),
]


def load_data(**kwargs: Any) -> None:
    """ load data into the database """
    conn = psycopg.connect(**kwargs)

    kernel_version = "1.2.3-1"
    kernel_uname = "4.3.2-5-amd64"
    release = "kinfo-test"
    architecture = "amd64"

    c = conn.cursor()     # pylint: disable=no-member
    kernel_id = importer._get_new_kernel_id(
        c, kernel_version, kernel_uname, release, architecture
    )

    importer._get_new_kernel_id(c, "1.3.5-2", "4.3.2-1-amd64", release, architecture)

    for module in sample_data_module:
        for pciid in module["pciid"]:
            importer._register_module(c, kernel_id, module["module"])
            pcidata = importer.pciid(pciid)
            assert pcidata is not None
            importer._register_pci_alias(c, kernel_id, module["module"], pcidata)
        for filename in module["firmware"]:
            importer._register_firmware(c, kernel_id, module["module"], filename)

    for package, files in sample_data_firmware.items():
        for filename in files:
            importer._register_firmware_file(
                c, release, architecture, package, filename
            )

    for module_name, page in sample_data_wikimap:
        importer._register_wiki_link(c, module_name, page)

    for key, value in sample_data_kconfig:
        importer._register_kconfig(c, kernel_id, key, value)

    for vid, did, vname, dname in sample_data_pciids:
        importer._register_pci_device(c, vid, did, vname, dname)

    conn.commit()   # pylint: disable=no-member


##########################################################################
# Fixture setup for test suite

###########################################################
# Database fixtures

postgresql_templated_proc = factories.postgresql_proc(
    user="testkinfo",
    password="qwerty",
    dbname="testkinfo",
    port=None,
    load=[Path(find_module_data(setup_file)), load_data],
)

postgresql = factories.postgresql(
    "postgresql_templated_proc",
    dbname="testkinfo",
)


@pytest.fixture(name="kinfo_db")
def fixture_kinfo_db(request: pytest.FixtureRequest, postgresql: psycopg.Connection) -> None:
    # pylint: disable=redefined-outer-name
    request.cls.db = KinfoDB(db=postgresql)
    request.cls.connman = request.cls.db.connection
    request.cls.kernel_version = "1.2.3-1"
    request.cls.kernel_uname = "4.3.2-5-amd64"
    request.cls.release = "kinfo-test"
    request.cls.architecture = "amd64"


@pytest.fixture(name="postgresql_direct_access")
def fixture_postgresql_direct_access(request: pytest.FixtureRequest, postgresql: psycopg.Connection) -> None:
    # pylint: disable=redefined-outer-name
    request.cls.postgresql = postgresql  # pragma: no cover


empty_postgresql_templated_proc = factories.postgresql_proc(
    user="testkinfo",
    password="qwerty",
    dbname="testkinfo",
    port=None,
)

empty_postgresql = factories.postgresql(
    "empty_postgresql_templated_proc",
    dbname="testkinfo",
)


@pytest.fixture(name="empty_postgresql_direct_access")
def fixture_empty_postgresql_direct_access(request: pytest.FixtureRequest, empty_postgresql: psycopg.Connection) -> None:
    # pylint: disable=redefined-outer-name
    request.cls.postgresql = empty_postgresql


###########################################################


@pytest.fixture
def stdout(request: pytest.FixtureRequest, capsys: pytest.CaptureFixture) -> None:
    request.cls.capsys = capsys


@pytest.fixture
def clstmpdir(request: pytest.FixtureRequest, tmp_path: Path) -> None:
    request.cls.test_dir = tmp_path


###########################################################
# Mirror/archive fixtures
#
# Contents.gz files obtained form the mirror and stored locally
# They are cached for an hour (controlled by max_age above)


def find_test_kernel_deb(location: Path, *args: str) -> Path:
    """ find a test file that is located within the test suite """

    # note: a magic value of args[0] == "" gives the directory itself and not
    # the filename within it.
    pattern = None
    if args and "*" in args[0]:
        pattern = args[0]
    elif not args or args is None:
        pattern = "linux-image*.deb"

    if pattern:
        filenames = list(location.glob(pattern))
        if filenames:
            return sorted(filenames)[-1]
        raise FileNotFoundError(args)

    return location / Path(*args)

def fetch_kernel_package(cache: Path, force: bool = False) -> Path:  # pragma: no cover
    ok_mtime = datetime.datetime.now(datetime.UTC).timestamp() - max_age

    # path = find_test_kernel_deb(tmp_path, "")
    cache.mkdir(parents=True, exist_ok=True)

    try:
        filepath = find_test_kernel_deb(cache)
        if (
            not force
            and filepath
            and filepath.exists()
            and filepath.stat().st_mtime >= ok_mtime
        ):
            return filepath
    except FileNotFoundError:
        # that's ok, we now try to download it
        pass

    # hunt for a kernel on the list of packages
    # this is a gross hack but unless we re-implement
    # lots of apt here, it will have to do
    r = requests.get(kernel_index_page, timeout=timeout)
    r.raise_for_status()

    soup = bs4.BeautifulSoup(r.text, "html.parser")

    kernelre = re.compile(
        r"linux-image-\d\.\d+.\d+((?!bpo|cloud|rt|trunk).)*-amd64_.*_amd64.deb"
    )

    links = soup.find_all("a", href=True)
    kernel_links = [l["href"] for l in links if kernelre.match(l["href"])]
    filename = kernel_links[-1]

    r = requests.get(kernel_index_page + "/" + str(filename), timeout=timeout)
    r.raise_for_status()

    filepath = cache / filename
    with open(filepath, "wb") as fh:
        for chunk in r.iter_content(chunk_size=2048):
            fh.write(chunk)
    return filepath


def unpack_deb(
    deb: Union[str, Path], dest: Union[str, Path]
) -> Path:  # pragma: no cover
    retval = subprocess.check_call(["dpkg", "-x", str(deb), str(dest)])
    if retval != 0:
        raise OSError("Error unpacking .deb")
    return Path(dest)


@pytest.fixture(name="kernel_downloader", scope="session")
def fixture_kernel_downloader(request: pytest.FixtureRequest, local_cache_dir: Path) -> Iterable[Path]:
    # pylint: disable=unused-argument
    force = False
    yield fetch_kernel_package(local_cache_dir, force)


@pytest.fixture(name="kernel_deb", scope="class")
def fixture_kernel_deb(request: pytest.FixtureRequest, kernel_downloader: Path) -> None:
    # pylint: disable=unused-argument
    request.cls.kernel_deb = kernel_downloader
    dest = kernel_downloader.with_suffix(".unpacked")
    dest = unpack_deb(request.cls.kernel_deb,  dest)
    request.cls.kernel_deb_unpacked = dest


LOCAL_CACHE_LOCATION: Optional[Path] = None


@pytest.fixture(name="local_cache_dir", scope="session")
def fixture_local_cache_dir(tmp_path_factory: pytest.TempPathFactory) -> Iterable[Path]:
    global LOCAL_CACHE_LOCATION    # pylint: disable=global-statement
    if not LOCAL_CACHE_LOCATION:
        # first attempt is to use a persistent directory within the source
        cache_dir = Path(__file__).parent.parent.parent / ".kinfo_test_cache"
        try:
            cache_dir.mkdir(parents=True, exist_ok=True)
        except PermissionError:
            cache_dir = tmp_path_factory.mktemp("kinfo_test_cache", numbered=False)
        LOCAL_CACHE_LOCATION = cache_dir

    yield LOCAL_CACHE_LOCATION
