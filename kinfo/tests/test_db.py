# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import pytest

import psycopg

import kinfo.db


@pytest.mark.usefixtures("empty_postgresql_direct_access")
class TestKinfoDBTests:

    postgresql: psycopg.Connection

    def testCreate(self) -> None:
        db = kinfo.db.KinfoDB(db=self.postgresql)
        with db.connection() as conn:
            assert conn is not None

    def testCreateDatabase(self) -> None:
        db = kinfo.db.KinfoDB(db=self.postgresql)
        with db.connection() as conn:
            kinfo.db.create_tables(conn)

            c = conn.cursor()
            for table in [
                "kernels",
                "modules",
                "pciids",
                "pci_devices",
                "kconfig",
                "firmware",
                "firmware_packages",
                "wiki_map",
            ]:
                c.execute(
                    """
                    SELECT
                        table_name, column_name, data_type
                    FROM
                        information_schema.columns
                    WHERE
                        table_name = %(table)s
                    """,
                    {"table": table},
                )
                cols = c.fetchall()
                assert len(cols) > 1

    def testCleanOrphans(self) -> None:
        """ Test that foreign key constraints are set correctly in the db """
        db = kinfo.db.KinfoDB(db=self.postgresql)
        with db.connection() as conn:
            kinfo.db.create_tables(conn)
            c = conn.cursor()
            c.execute("INSERT INTO kinfodb.kernels (id) VALUES (1), (2), (3), (5)")
            c.execute("INSERT INTO kinfodb.modules (kernel_id) VALUES (1), (3)")
            c.execute("INSERT INTO kinfodb.pciids (kernel_id) VALUES (1), (3)")
            c.execute("INSERT INTO kinfodb.firmware (kernel_id) VALUES (1), (2), (5)")
            c.execute("INSERT INTO kinfodb.kconfig (kernel_id) VALUES (2), (3)")

            c.execute("SELECT COUNT(*) FROM kinfodb.pciids")
            data = c.fetchone()
            assert data is not None
            assert data[0] == 2

            # Now remove some kernels and make sure that the everything is
            # set to clean up the dependent tables
            c.execute("DELETE FROM kinfodb.kernels WHERE id = 3 or id = 5")

            c.execute("SELECT COUNT(*) FROM kinfodb.modules")
            data = c.fetchone()
            assert data is not None
            assert data[0] == 1
            c.execute("SELECT COUNT(*) FROM kinfodb.pciids")
            data = c.fetchone()
            assert data is not None
            assert data[0] == 1
            c.execute("SELECT COUNT(*) FROM kinfodb.firmware")
            data = c.fetchone()
            assert data is not None
            assert data[0] == 2
            c.execute("SELECT COUNT(*) FROM kinfodb.kconfig")
            data = c.fetchone()
            assert data is not None
            assert data[0] == 1
