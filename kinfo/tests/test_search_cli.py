# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import logging
import shutil
import subprocess
from unittest.mock import ANY, Mock, patch

from typing import Callable

# from psycopg import Connection
import pytest

from kinfo import search_cli
from kinfo.db import KinfoDB
from ._util import _StdOutTestMixin


# work around mypy not understanding Connection as a context manager:
#  error: "Connection[Any]" not callable  [operator]
Connection = Callable


sample_lspci_output = b"""\
00:00.0 "0600" "14E4" "163C" -r08 "103c" "8079"
00:02.0 "0300" "8086" "0089" -r07 "103c" "8079"
"""


@pytest.mark.usefixtures("kinfo_db")
class TestsSearchCli(_StdOutTestMixin):

    # from the kinfo_db fixture
    db: KinfoDB
    connman: Connection
    kernel_version: str
    kernel_uname: str
    release: str
    architecture: str

    def test_show_modinfo(self) -> None:
        with self.connman() as conn:
            with self.assertInStdOut(["000014E4:0000163C", "bnx2-mips-09-6.2.1b.fw"]):
                assert search_cli.show_modinfo(
                        conn, self.release, self.architecture, ["bnx2"]
                    )

            with self.assertInStdOut(["000014E4:0000163C", "bnx2-mips-09-6.2.1b.fw"]):
                assert search_cli.show_modinfo(
                        conn, self.release, self.architecture, "bnx2"
                    )

            # check down-casing of - to _ in searches.
            with self.assertInStdOut(["8086"]):
                assert search_cli.show_modinfo(
                        conn, self.release, self.architecture, "snd-hda-intel"
                    )

            with self.assertInStdOut(["8086"]):
                assert search_cli.show_modinfo(
                        conn, self.release, self.architecture, "snd_hda_intel"
                    )

            assert not search_cli.show_modinfo(
                    conn, self.release, self.architecture, ["no-such-module"]
                )

            # non-existent kernel
            assert not search_cli.show_modinfo(conn, self.release, "no-such-arch", ["bnx2"])
            assert not search_cli.show_modinfo(
                    conn, "no-such-release", self.architecture, ["bnx2"]
                )

    def test_show_firmware(self) -> None:
        with self.connman() as conn:
            with self.assertInStdOut(["Firmware", "firmware-bnx2"]):
                assert search_cli.show_firmware(
                        conn,
                        self.release,
                        self.architecture,
                        ["bnx2-rv2p-09-6.0.17.fw"],
                    )

            with self.assertInStdOut(["Firmware", "firmware-bnx2"]):
                assert search_cli.show_firmware(
                        conn, self.release, self.architecture, "bnx2-rv2p-09-6.0.17.fw"
                    )

            with self.assertInStdOut(["cannot be found in any packages"]):
                assert not search_cli.show_firmware(
                        conn, self.release, self.architecture, ["no-such-firmware"]
                    )

            with self.assertInStdOut(["Error"]):
                # non-existent kernel
                assert not search_cli.show_firmware(
                        conn, self.release, "no-such-arch", ["bnx2-rv2p-09-6.0.17.fw"]
                    )

            with self.assertInStdOut(["Error"]):
                assert not search_cli.show_firmware(
                        conn,
                        "no-such-release",
                        self.architecture,
                        ["bnx2-rv2p-09-6.0.17.fw"],
                    )

    def test_show_pciid(self) -> None:
        with self.connman() as conn:
            with self.assertInStdOut(["NetXtreme", "Broadcom", "firmware-bnx2"]):
                assert search_cli.show_pciid(
                        conn, self.release, self.architecture, ["14E4:163C"]
                    )

            with self.assertInStdOut(["NetXtreme", "Broadcom", "firmware-bnx2"]):
                assert search_cli.show_pciid(
                        conn, self.release, self.architecture, "14E4:163C"
                    )

            with self.assertInStdOut(["Error"]):
                assert not search_cli.show_pciid(
                        conn, self.release, self.architecture, ["asfasdf:asdfasdf"]
                    )

            with self.assertInStdOut(["not in the PCI Id database"]):
                assert not search_cli.show_pciid(
                        conn, self.release, self.architecture, ["1401:0101"]
                    )

            # non-existent kernel
            with self.assertInStdOut(["Error"]):
                assert not search_cli.show_pciid(
                        conn, self.release, "no-such-arch", ["14E4:163C"]
                    )

            with self.assertInStdOut(["Error"]):
                assert not search_cli.show_pciid(
                        conn, "no-such-release", self.architecture, ["14E4:163C"]
                    )

    def test_show_kernels_summary(self) -> None:
        with self.connman() as conn:
            with self.assertInStdOut([self.release, self.kernel_uname]):
                assert search_cli.show_kernels_summary(
                        conn, self.release, self.architecture
                    )

            with self.assertInStdOut(absent_needles=[self.release, self.kernel_uname]):
                assert not search_cli.show_kernels_summary(
                        conn, "no-release", self.architecture
                    )

            with self.assertInStdOut(absent_needles=[self.release, self.kernel_uname]):
                assert not search_cli.show_kernels_summary(conn, self.release, "no-such-arch")

    def test_show_kconfig(self) -> None:
        with self.connman() as conn:
            with self.assertInStdOut(["BNX2", "m"]):
                assert search_cli.show_kconfig(
                        conn, self.release, self.architecture, ["BNX2"]
                    )

            with self.assertInStdOut(["BNX2", "m"]):
                assert search_cli.show_kconfig(
                        conn, self.release, self.architecture, "BNX2"
                    )

            with self.assertInStdOut(["BNX2", "BNX2X"]):
                assert search_cli.show_kconfig(
                        conn, self.release, self.architecture, ["%BNX2%"]
                    )

            with self.assertInStdOut(absent_needles=["Error"]):
                assert not search_cli.show_kconfig(
                        conn, self.release, self.architecture, ["no-such-config"]
                    )

            with self.assertInStdOut(["Error"]):
                assert not search_cli.show_kconfig(
                        conn, "no-such-release", self.architecture, ["BNX2"]
                    )

            with self.assertInStdOut(["Error"]):
                assert not search_cli.show_kconfig(
                        conn, self.release, "no-such-architecture", ["BNX2"]
                    )

    def test_show_pciname_matches(self) -> None:
        with self.connman() as conn:
            with self.assertInStdOut(["00008086:00000089", "Centrino"]):
                assert search_cli.show_pciname_matches(conn, "%Centri%")

            with self.assertInStdOut(
                ["000014E4:0000163C", "000014E4:0000163D", "NetXtreme"]
            ):
                assert search_cli.show_pciname_matches(conn, "%NetX%")

            with self.assertInStdOut(["00008086:00000089", "Centrino"]):
                assert search_cli.show_pciname_matches(conn, "Centr%")

            with self.assertInStdOut(absent_needles=["Centrino"]):
                assert not search_cli.show_pciname_matches(conn, "%Centr")

            with self.assertInStdOut(["NetXtreme"]):
                assert search_cli.show_pciname_matches(conn, "NetXt")

            assert not search_cli.show_pciname_matches(conn, "no-such-device")

    @pytest.mark.skipif(not shutil.which("lspci"), reason="requires lspci")
    @patch("subprocess.check_output", wraps=subprocess.check_output)
    def test_show_lspci(self, mock_check_output: Mock, caplog: pytest.LogCaptureFixture) -> None:
        with self.connman() as conn:
            mock_check_output.reset_mock()
            assert search_cli.show_lspci(
                    conn, self.release, self.architecture, False, False
                )
            mock_check_output.assert_any_call(["lspci", ANY])

            with caplog.at_level(logging.ERROR):
                search_cli.show_lspci(conn, None, self.architecture, True, False)
                assert "not specified" in caplog.text

            with caplog.at_level(logging.ERROR):
                search_cli.show_lspci(conn, self.release, None, False, True)
                assert "not specified" in caplog.text

    @patch("subprocess.check_output", return_value=sample_lspci_output)
    def test_show_lspci_firmware(self, mock_check_output: Mock, caplog: pytest.LogCaptureFixture) -> None:
        with self.connman() as conn:
            mock_check_output.reset_mock()
            with self.assertInStdOut(["[14E4:163C]", "NetXtreme"]):
                assert search_cli.show_lspci(
                        conn, self.release, self.architecture, True, True
                    )

            with caplog.at_level(logging.WARNING):
                mock_check_output.reset_mock()
                search_cli.show_lspci(
                    conn, "no-such-release", self.architecture, True, False
                )
                assert "modules lookup error" in caplog.text

            with caplog.at_level(logging.WARNING):
                mock_check_output.reset_mock()
                search_cli.show_lspci(conn, self.release, "no-such-arch", False, True)
                assert "modules lookup error" in caplog.text
