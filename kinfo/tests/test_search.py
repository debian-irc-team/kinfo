# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

from typing import Callable
# from psycopg import Connection

import pytest

from kinfo import search
from kinfo.db import KinfoDB


# work around mypy not understanding Connection as a context manager:
#  error: "Connection[Any]" not callable  [operator]
Connection = Callable


@pytest.mark.usefixtures("kinfo_db")
class TestSearch:

    # from the kinfo_db fixture
    db: KinfoDB
    connman: Connection
    kernel_version: str
    kernel_uname: str
    release: str
    architecture: str

    def test_check_kernel_exists(self) -> None:
        with self.connman() as conn:
            c = conn.cursor()
            assert search.check_kernel_exists(c, self.release, self.architecture)
            assert not search.check_kernel_exists(
                    c, self.release, "no-such-arch", raises=False
                )
            assert not search.check_kernel_exists(
                    c, "no-such-release", self.architecture, raises=False
                )
            with pytest.raises(ValueError):
                assert not search.check_kernel_exists(c, self.release, "no-such-arch")
            with pytest.raises(ValueError):
                assert not search.check_kernel_exists(c, "no-such-release", self.architecture)

    def test_check_module_exists(self) -> None:
        with self.connman() as conn:
            c = conn.cursor()
            assert search.check_module_exists(c, self.release, self.architecture, "bnx2")
            assert search.check_module_exists(
                    c, self.release, self.architecture, "snd-hda-intel"
                )
            assert search.check_module_exists(
                    c, self.release, self.architecture, "snd_hda_intel"
                )
            assert not search.check_module_exists(
                    c, self.release, "no-such-arch", "bnx2", raises=False
                )
            assert not search.check_module_exists(
                    c, "no-such-release", self.architecture, "bnx2", raises=False
                )
            assert not search.check_module_exists(
                    c, self.release, self.architecture, "no-such-module", raises=False
                )
            with pytest.raises(ValueError):
                assert not search.check_module_exists(c, self.release, "no-such-arch", "bnx2")
            with pytest.raises(ValueError):
                assert not search.check_module_exists(
                        c, "no-such-release", self.architecture, "bnx2"
                    )
            with pytest.raises(ValueError):
                assert not search.check_module_exists(
                        c, self.release, self.architecture, "no-such-module"
                    )

    def test_pciid_lookup(self) -> None:
        with self.connman() as conn:
            # upper case
            modules = search.pciid_lookup(
                conn,
                self.release,
                self.architecture,
                vendor="14E4",
                device="163C",
            )
            assert modules == ["bnx2"]

            # lower case
            modules = search.pciid_lookup(
                conn,
                self.release,
                self.architecture,
                vendor="14e4",
                device="163c",
            )
            assert modules == ["bnx2"]

            # no such module
            modules = search.pciid_lookup(
                conn,
                self.release,
                self.architecture,
                vendor="1234",
                device="1234",
            )
            assert modules == []

            # badly formatted
            with pytest.raises(ValueError):
                search.pciid_lookup(
                    conn,
                    self.release,
                    self.architecture,
                    vendor="1234asdf",
                    device="1234asdf",
                )

            # upper case
            modules = search.pciid_lookup(
                conn,
                self.release,
                self.architecture,
                vendor="14E4",
                device="163C",
                wildcards=False,
            )
            assert modules == ["bnx2"]

            # non-existent kernel
            with pytest.raises(ValueError):
                search.pciid_lookup(
                    conn,
                    self.release,
                    "no-such-arch",
                    vendor="14E4",
                    device="163C",
                )

            with pytest.raises(ValueError):
                search.pciid_lookup(
                    conn,
                    "no-such-release",
                    self.architecture,
                    vendor="14E4",
                    device="163C",
                )

    def test_pciid_lookup_from_pciid(self) -> None:
        with self.connman() as conn:
            modules = search.pciid_lookup(
                conn, self.release, self.architecture, pciid="14E4:163C"
            )
            assert modules == ["bnx2"]

            # badly formatted pciid
            with pytest.raises(ValueError):
                modules = search.pciid_lookup(
                    conn, self.release, self.architecture, pciid="asdfasdf"
                )

            # non-existent kernel
            with pytest.raises(ValueError):
                search.pciid_lookup(
                    conn, self.release, "no-such-arch", pciid="14E4:163C"
                )
            with pytest.raises(ValueError):
                search.pciid_lookup(
                    conn, "no-such-release", self.architecture, pciid="14E4:163C"
                )

    def test_pciname_lookup(self) -> None:
        with self.connman() as conn:
            devices = search.pciname_match_lookup(conn, r"%NetXtreme%")
            assert len(devices) == 4
            assert devices[0][0] == "000014E4"
            assert "Broad" in devices[0][2]
            assert "BCM5716S" in devices[0][3]

            devices = search.pciname_match_lookup(conn, "Cent%")
            assert len(devices) == 1
            assert devices[0][1] == "00000089"

            # no wildcards means no match
            devices = search.pciname_match_lookup(conn, "Centrino")
            assert len(devices) == 0

            # non-existent
            devices = search.pciname_match_lookup(conn, "no-such-device")
            assert len(devices) == 0

    def test_module_lookup(self) -> None:
        with self.connman() as conn:
            pcimatches = search.module_lookup(
                conn, self.release, self.architecture, "bnx2"
            )
            assert len(pcimatches) == 2
            assert len(pcimatches[0]) == 2

            with pytest.raises(ValueError):
                search.module_lookup(
                    conn, self.release, self.architecture, "no-such-module"
                )

            with pytest.raises(ValueError):
                search.module_lookup(conn, self.release, "no-such-arch", "bnx2")

            with pytest.raises(ValueError):
                search.module_lookup(conn, "no-such-release", self.architecture, "bnx2")

    def test_kernel_lookup(self) -> None:
        with self.connman() as conn:
            kernels = search.kernel_lookup(conn, self.release, self.architecture)
            assert len(kernels) == 2

            kernels = search.kernel_lookup(
                conn, "no-such-release", "no-such-architecture"
            )
            assert len(kernels) == 0

            kernels = search.kernel_lookup(conn, None, self.architecture)
            assert len(kernels) == 2

    def test_firmware_lookup(self) -> None:
        with self.connman() as conn:
            filenames = search.firmware_lookup(
                conn, self.release, self.architecture, "bnx2"
            )
            assert len(filenames) == 3
            assert "bnx2-rv2p-09-6.0.17.fw" in filenames

            filenames = search.firmware_lookup(
                conn, self.release, self.architecture, "bnx2nofw"
            )
            assert len(filenames) == 0

            # non-existent kernel
            with pytest.raises(ValueError):
                search.firmware_lookup(conn, self.release, "no-such-arch", "bnx2")
            with pytest.raises(ValueError):
                search.firmware_lookup(
                    conn, "no-such-release", self.architecture, "bnx2"
                )
            with pytest.raises(ValueError):
                search.firmware_lookup(
                    conn, self.release, self.architecture, "no-such-module"
                )

    def test_firmware_packages_lookup(self) -> None:
        with self.connman() as conn:
            packages = search.firmware_packages_lookup(
                conn, self.release, self.architecture, "bnx2-rv2p-09-6.0.17.fw"
            )
            assert len(packages) == 1
            assert "firmware-bnx2" in packages

            packages = search.firmware_packages_lookup(
                conn, self.release, self.architecture, "no-such-firmware"
            )
            assert len(packages) == 0

            # non-existent kernel
            with pytest.raises(ValueError):
                search.firmware_packages_lookup(
                    conn, self.release, "no-such-arch", "bnx2-rv2p-09-6.0.17.fw"
                )
            with pytest.raises(ValueError):
                search.firmware_packages_lookup(
                    conn, "no-such-release", self.architecture, "bnx2-rv2p-09-6.0.17.fw"
                )

    def test_kconfig_lookup(self) -> None:
        with self.connman() as conn:
            # test case insensitivity
            kconfig = search.kconfig_lookup(
                conn, self.release, self.architecture, "bnx2"
            )
            assert len(kconfig) == 1
            assert kconfig[0][0] == "BNX2"
            assert kconfig[0][1] == "m"

            # test CONFIG prefix stripping
            kconfig = search.kconfig_lookup(
                conn, self.release, self.architecture, "CONFIG_BNX2"
            )
            assert len(kconfig) == 1
            assert kconfig[0][0] == "BNX2"
            assert kconfig[0][1] == "m"

            # test wildcard searching
            kconfig = search.kconfig_lookup(
                conn, self.release, self.architecture, "%bnx2%"
            )
            assert len(kconfig) == 4

            # test not found
            kconfig = search.kconfig_lookup(
                conn, self.release, self.architecture, "no-such-config"
            )
            assert len(kconfig) == 0

            # non-existent kernel
            with pytest.raises(ValueError):
                search.kconfig_lookup(conn, self.release, "no-such-arch", "bnx2")
            with pytest.raises(ValueError):
                search.kconfig_lookup(
                    conn, "no-such-release", self.architecture, "bnx2"
                )

    def test_fetch_pciid_match(self) -> None:
        with self.connman() as conn:
            modules, packages = search.fetch_pciid_match(
                conn, self.release, self.architecture, "14E4:163C"
            )
            assert "bnx2" in modules
            assert "firmware-bnx2" in packages

            modules, packages = search.fetch_pciid_match(
                conn, self.release, self.architecture, "1234:4321"
            )
            assert modules == []
            assert packages == []

            with pytest.raises(ValueError):
                modules, packages = search.fetch_pciid_match(
                    conn, self.release, self.architecture, "asfasdfa"
                )

            # non-existent kernel
            with pytest.raises(ValueError):
                search.fetch_pciid_match(
                    conn, self.release, "no-such-arch", "1234:4321"
                )
            with pytest.raises(ValueError):
                search.fetch_pciid_match(
                    conn, "no-such-release", self.architecture, "1234:4321"
                )

    def test_wiki_match_lookup(self) -> None:
        with self.connman() as conn:
            pages = search.wiki_match_lookup(conn, "iwlwifi")
            assert pages == ["iwlwifi"]

            pages = search.wiki_match_lookup(conn, "foo")
            assert pages == ["bar"]

            pages = search.wiki_match_lookup(conn, "no-such-entry")
            assert pages == []

    def test_pciid_name_lookup(self) -> None:
        with self.connman() as conn:
            vname, dname = search.pciid_name_lookup(conn, pciid="14E4:163C")
            assert vname == "Broadcom Corporation"
            assert dname == "NetXtreme II BCM5716S Gigabit Ethernet"

            vname, dname = search.pciid_name_lookup(conn, vendor="14E4", device="163C")
            assert vname == "Broadcom Corporation"
            assert dname == "NetXtreme II BCM5716S Gigabit Ethernet"

            vname, dname = search.pciid_name_lookup(conn, vendor="1234", device="5678")
            assert vname is None
            assert dname is None

            with pytest.raises(ValueError):
                search.pciid_name_lookup(
                    conn, pciid="14E4:163C", vendor="14E4", device="163C"
                )

            with pytest.raises(ValueError):
                search.pciid_name_lookup(conn, pciid="asdf:asdf")

            with pytest.raises(ValueError):
                search.pciid_name_lookup(conn, vendor="asdf", device="asdf")


class TestSearchNonDb:
    def test_clean_pciid_options(self) -> None:
        """Test overloads of the clean_pciid_options function"""
        v, d = search.clean_pciid_options(vendor="1234", device="5678", wide=True)
        assert v == "00001234"
        assert d == "00005678"

        v, d = search.clean_pciid_options(vendor="1234", device="5678", wide=False)
        assert v == "1234"
        assert d == "5678"

        v, d = search.clean_pciid_options(pciid="1234:5678", wide=True)
        assert v == "00001234"
        assert d == "00005678"

        v, d = search.clean_pciid_options(pciid="1234:5678", wide=False)
        assert v == "1234"
        assert d == "5678"

        with pytest.raises(ValueError):
            # FIXME: this should generate a mypy type error
            search.clean_pciid_options(vendor="1234", device="5678", pciid="1234:5678")

        with pytest.raises(ValueError):
            # FIXME: this should generate a mypy type error
            search.clean_pciid_options(vendor="1234")

        with pytest.raises(ValueError):
            search.clean_pciid_options(vendor="1234", device="asdf")

        with pytest.raises(ValueError):
            search.clean_pciid_options(vendor="asdf", device="1234")

        with pytest.raises(ValueError):
            search.clean_pciid_options(pciid="1234:asdf")

        with pytest.raises(ValueError):
            search.clean_pciid_options(pciid="asdf:1234")

    def test_pciid_check_format(self) -> None:
        assert search.pciid_check_format("1234:5678")
        assert search.pciid_check_format("00001234:00005678")
        assert search.pciid_check_format("dead:beef")
        assert search.pciid_check_format("DEAD:BEEF")
        assert not search.pciid_check_format("234:5678")
        assert not search.pciid_check_format("12345:5678")
        assert not search.pciid_check_format("1234:56789")
        assert not search.pciid_check_format("asdf:5678")
        assert not search.pciid_check_format("1234:asdf")

    def test_pciid_split(self) -> None:
        assert search.pciid_split("1234:5678"), ("1234", "5678")
        assert search.pciid_split("00001234:00005678"), ("00001234", "00005678")
        assert search.pciid_split("1234:asdf"), (None, None)
        assert search.pciid_split("asfd:5678"), (None, None)
