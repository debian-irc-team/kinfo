# Copyright (c) 2014-2021  Stuart Prescott
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

from contextlib import contextmanager
import logging
import os
from pathlib import Path
import shutil
import tempfile
from unittest.mock import Mock, patch

from typing import Any, Callable, Generator, Iterable, List, Optional

import pytest

from psycopg import Cursor

from pydebcontents import ContentsDict

from kinfo import importer
from kinfo.db import KinfoDB


logger = logging.getLogger()


# work around mypy not understanding Connection as a context manager:
#  error: "Connection[Any]" not callable  [operator]
Connection = Callable


modinfo_output = """\
filename:       /lib/modules/3.16.0-4-amd64/kernel/drivers/net/ethernet/broadcom/bnx2.ko
firmware:       bnx2/bnx2-rv2p-09ax-6.0.17.fw
firmware:       bnx2/bnx2-rv2p-09-6.0.17.fw
firmware:       bnx2/bnx2-mips-09-6.2.1b.fw
firmware:       bnx2/bnx2-rv2p-06-6.0.15.fw
firmware:       bnx2/bnx2-mips-06-6.2.3.fw
version:        2.2.5
license:        GPL
description:    Broadcom NetXtreme II BCM5706/5708/5709/5716 Driver
author:         Michael Chan <mchan@broadcom.com>
srcversion:     7D480010E9C52810EBEBFAF
alias:          pci:v000014E4d0000163Csv*sd*bc*sc*i*
alias:          pci:v000014E4d0000163Bsv*sd*bc*sc*i*
alias:          pci:v000014E4d0000163Asv*sd*bc*sc*i*
alias:          pci:v000014E4d00001639sv*sd*bc*sc*i*
alias:          pci:v000014E4d000016ACsv*sd*bc*sc*i*
alias:          pci:v000014E4d000016AAsv*sd*bc*sc*i*
alias:          pci:v000014E4d000016AAsv0000103Csd00003102bc*sc*i*
alias:          pci:v000014E4d0000164Csv*sd*bc*sc*i*
alias:          pci:v000014E4d0000164Asv*sd*bc*sc*i*
alias:          pci:v000014E4d0000164Asv0000103Csd00003106bc*sc*i*
alias:          pci:v000014E4d0000164Asv0000103Csd00003101bc*sc*i*
depends:
intree:         Y
vermagic:       3.16.0-4-amd64 SMP mod_unload modversions
parm:           disable_msi:Disable Message Signaled Interrupt (MSI) (int)
"""
modinfo_output = "\0".join(s for s in modinfo_output.splitlines())

modinfo_output_sections = set(
    (
        "name",
        "filename",
        "firmware",
        "license",
        "description",
        "author",
        "alias",
        "depends",
        "intree",
        "vermagic",
        "parm",
    )
)

pciid_sample = """
#       List of PCI ID's
14e4  Broadcom Corporation
\t163c  NetXtreme II BCM5716S Gigabit Ethernet
\t163d  NetXtreme II BCM57811 10-Gigabit Ethernet
\t163e  NetXtreme II BCM57811 10 Gigabit Ethernet Multi Function
\t163f  NetXtreme II BCM57811 10-Gigabit Ethernet Virtual Function
8086  Intel Corporation
\t0089  Centrino Advanced-N + WiMAX 6250 [Kilmer Peak]
\t\t8086 1311  Centrino Advanced-N + WiMAX 6250 2x2 AGN
\t\t8086 1316  Centrino Advanced-N + WiMAX 6250 2x2 ABG
"""

data_contents_sample = b"""
lib/firmware/bnx2/bnx2-mips-06-5.0.0.j3.fw              non-free/kernel/firmware-bnx2
lib/firmware/bnx2/bnx2-mips-06-6.2.1.fw                 non-free/kernel/firmware-bnx2
lib/firmware/bnx2/bnx2-mips-06-6.2.3.fw                 non-free/kernel/firmware-bnx2
"""

wiki_map_sample = """
#language en
= Comments and boilerplate =

 * iwl3945 [[iwlegacy]]
 * iwl4965 [[iwlegacy]]
 * iwlagn [[iwlwifi]]
 * iwlwifi [[iwlwifi]]

"""


def _find_sample_file(base: Path, pattern: str) -> Optional[Path]:
    files = _find_sample_files(base, pattern)
    sample_file = files[0] if files else None
    return sample_file


def _find_sample_files(base: Path, pattern: str) -> Optional[List[Path]]:
    files = list(base.glob(pattern)) + list(base.glob("usr/" + pattern))
    if files:
        return files
    return None  # pragma: no cover


@pytest.mark.usefixtures("kinfo_db")
@pytest.mark.usefixtures("clstmpdir")
@pytest.mark.usefixtures("kernel_deb")
class TestImporter:

    # from the kinfo_db fixture
    db: KinfoDB
    connman: Connection
    kernel_version: str
    kernel_uname: str
    release: str
    architecture: str

    # from the clstmpdir fixture
    test_dir: Path

    # from the kernel_deb fixture
    kernel_deb: Path
    kernel_deb_unpacked: Path

    def assertSubset(self, set1: Iterable[Any], set2: Iterable[Any]) -> None:
        """
        asserts that all elements of set1 are in set2
        """
        not_found = [i for i in set1 if i not in set2]
        assert not_found == [], "Extra elements in set"

    @contextmanager
    def temporary_kernel_id(
        self,
        cursor: Cursor,
        release: Optional[str] = None,
        architecture: Optional[str] = None,
        version: Optional[str] = None,
        uname: Optional[str] = None,
    ) -> Generator[int, None, None]:
        release = release or self.release
        architecture = architecture or self.architecture
        version = version or self.kernel_version
        uname = uname or self.kernel_uname

        try:
            kernel_id = importer._get_new_kernel_id(
                cursor, version, uname, release, architecture
            )
            assert kernel_id >= 1
            yield kernel_id

        finally:
            importer._remove_kernels(cursor, kernel_id)

    def test_kernel_package(self) -> None:
        filename = "linux-image-3.16.0-4-amd64_3.16.7-ckt9-3~deb8u1_amd64.deb"
        kp = importer.kernel_package(filename)
        assert kp.filename == filename
        assert kp.uname == "3.16.0-4-amd64"
        assert kp.version == "3.16.7-ckt9-3~deb8u1"
        assert kp.architecture == "amd64"

        with pytest.raises(ValueError):
            filename = "my-random-deb_1.2.3-1_amd64.deb"
            importer.kernel_package(filename)

    def test_kernel_indexed(self) -> None:
        version = "1.2.3-ckt9-3~deb8u1"
        uname = "1.2.3-4-test"
        release = "foo"
        arch = "test"

        with self.connman() as conn:
            c = conn.cursor()
            assert -1 == importer.kernel_indexed(c, version, uname, release, arch)
            kernel_id = importer._get_new_kernel_id(c, version, uname, release, arch)
            assert kernel_id == importer.kernel_indexed(c, version, uname, release, arch)
            assert -1 == importer.kernel_indexed(c, version, "no-such-uname", "", "")

    def test_find_modules(self) -> None:
        mod_path = _find_sample_file(self.kernel_deb_unpacked, "lib/modules/*")
        assert mod_path is not None
        output = importer.find_modules(mod_path)
        assert len(output) >= 100
        bnx2_mods = [m for m in output if "bnx2" in str(m)]
        assert len(bnx2_mods) >= 2
        assert len(bnx2_mods) <= 10

    def test_modinfo_tool_installed(self) -> None:
        tool = shutil.which(importer.modinfo_tool)
        assert tool, "Ensure that modinfo is installed (kmod package)"

    def test_modinfo(self) -> None:
        mod_file = _find_sample_file(
            self.kernel_deb_unpacked,
            "lib/modules/*/kernel/drivers/net/ethernet/broadcom/bnx2.ko*"
        )
        assert mod_file is not None
        output = importer.modinfo(mod_file)
        self.assertSubset(modinfo_output_sections, set(output.keys()))
        assert output["name"] == ["bnx2"]
        assert len(output["firmware"]) >= 1

    def test_parse_modinfo(self) -> None:
        output = importer._parse_modinfo(modinfo_output, "bnx2")
        self.assertSubset(modinfo_output_sections, set(output.keys()))
        assert output["name"] == ["bnx2"]
        assert len(output["firmware"]) >= 1

    def test_pciid(self) -> None:
        data = importer.pciid("pci:v000014E4d0000163Csv*sd*bc*sc*i*")
        assert data is not None
        assert data.v == "000014E4"
        assert data.d == "0000163C"
        assert data.sv == "*"

    def test_import_modules(self) -> None:
        modules = _find_sample_files(
            self.kernel_deb_unpacked,
            "lib/modules/*/kernel/drivers/net/ethernet/broadcom/*.ko*"
        )
        assert modules is not None
        with self.connman() as conn:
            c = conn.cursor()
            with self.temporary_kernel_id(c, release="test-importer") as kernel_id:
                importer.import_modules(c, kernel_id, modules)
                c.execute(
                    "SELECT COUNT(*) FROM kinfodb.pciids WHERE kernel_id=%s",
                    (kernel_id,),
                )
                assert c.fetchone()[0] >= 50
                c.execute(
                    "SELECT COUNT(*) FROM kinfodb.firmware WHERE kernel_id=%s",
                    (kernel_id,),
                )
                assert c.fetchone()[0] >= 5

    def test_import_all_modules(self) -> None:
        with tempfile.TemporaryDirectory(prefix="tmp-test-import-") as tmpdir:
            source = _find_sample_file(
                self.kernel_deb_unpacked,
                "lib/modules/*/kernel/drivers/net/ethernet/broadcom"
            )
            assert source is not None
            shutil.copytree(source, os.path.join(tmpdir, "modules"))
            with self.connman() as conn:
                c = conn.cursor()
                with self.temporary_kernel_id(c, release="test-importer") as kernel_id:
                    importer.import_all_modules(c, kernel_id, tmpdir)
                    c.execute(
                        "SELECT COUNT(*) FROM kinfodb.pciids WHERE kernel_id=%s",
                        (kernel_id,),
                    )
                    assert c.fetchone()[0] >= 50
                    c.execute(
                        "SELECT COUNT(*) FROM kinfodb.firmware WHERE kernel_id=%s",
                        (kernel_id,),
                    )
                    assert c.fetchone()[0] >= 5

    def test_find_config(self) -> None:
        config = importer.find_config("/", "3.16.0-4-amd64")
        assert "3.16.0-4-amd64" in str(config)
        assert "config" in str(config)
        config = importer.find_config(os.getcwd(), "3.16.0-4-amd64")
        assert "3.16.0-4-amd64" in str(config)
        assert "config" in str(config)
        assert os.getcwd() in str(config)

    def test_load_config_file(self) -> None:
        filename = _find_sample_file(self.kernel_deb_unpacked, "boot/config-*")
        assert filename is not None
        conf = importer.load_config_file(filename)
        assert len(conf) >= 1000
        assert len([c for c in conf if c[1] == "y"]) >= 1000
        assert len([c for c in conf if c[1] == "n"]) >= 1000
        assert len([c for c in conf if c[1] == "m"]) >= 1000

    def test_import_kconfig(self) -> None:
        with self.connman() as conn:
            c = conn.cursor()
            conf = _find_sample_file(self.kernel_deb_unpacked, "boot/config-*")
            assert conf is not None
            rootdir = conf.parent.parent
            uname = conf.name.partition("-")[-1]
            with self.temporary_kernel_id(
                c, release="test-importer", uname=uname
            ) as kernel_id:
                importer.import_kconfig(c, kernel_id, uname, rootdir)
                c.execute(
                    "SELECT COUNT(*) FROM kinfodb.kconfig WHERE kernel_id=%s",
                    (kernel_id,),
                )
                assert c.fetchone()[0] >= 1000

    def test_get_new_kernel_id(self) -> None:
        with self.connman() as conn:
            c = conn.cursor()
            version = "1.2.3-ckt9-3~deb8u1"
            uname = "1.2.3-4-test"
            release = "foo"
            arch = "test"

            kernel_id = importer._get_new_kernel_id(c, version, uname, release, arch)

            c.execute("SELECT * FROM kinfodb.kernels WHERE version=%s", (version,))
            v = c.fetchone()
            assert v[0] == kernel_id
            assert v[1] == version
            assert v[2] == uname
            assert v[3] == release
            assert v[4] == arch

    def test_import_kernel(self) -> None:
        # find some modules to import
        version = "1.2.3-ckt9-3~deb8u1"
        uname = "1.2.3-4-test"
        release = "foo"
        arch = "test"

        tree = os.path.join(self.test_dir, "kernel")

        source = _find_sample_file(self.kernel_deb_unpacked, "lib/modules/*/kernel/drivers/net/ethernet/broadcom")
        assert source is not None
        shutil.copytree(source, os.path.join(tree, "lib", "modules", uname))
        os.makedirs(os.path.join(tree, "boot"))

        source = _find_sample_file(self.kernel_deb_unpacked, "boot/config-*")
        assert source is not None
        shutil.copy(source, importer.find_config(tree, uname))

        with self.connman() as conn:
            importer.import_kernel(conn, version, uname, release, arch, tree)

            c = conn.cursor()
            c.execute("SELECT id FROM kinfodb.kernels WHERE version=%s", (version,))
            kernel_id = c.fetchone()[0]

            # test that modules were imported
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.pciids WHERE kernel_id=%s", (kernel_id,)
            )
            assert c.fetchone()[0] >= 50
            # test that firmware was imported
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.firmware WHERE kernel_id=%s", (kernel_id,)
            )
            assert c.fetchone()[0] >= 5
            # test that config was imported
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.kconfig WHERE kernel_id=%s", (kernel_id,)
            )
            assert c.fetchone()[0] >= 1000

            # test reimport is skipped
            kid = importer.import_kernel(conn, version, uname, release, arch, tree)
            assert kid == -1

            # test reimport is forced
            kid = importer.import_kernel(
                conn, version, uname, release, arch, tree, True
            )
            assert kid > 0

    def test_import_deb(self) -> None:
        # test uses the kernel_deb fixture to fetch a kernel
        debfile = self.kernel_deb
        logger.info("Given kernel .deb: %s", debfile)
        assert debfile is not None
        kp = importer.kernel_package(debfile)
        logger.info("Imported as kernel: %s", str(kp))
        release = "test"
        with self.connman() as conn:
            importer.import_deb(conn, debfile, release, limit=400)

            c = conn.cursor()
            c.execute("SELECT id FROM kinfodb.kernels WHERE version=%s", (kp.version,))
            kernel_id = c.fetchone()[0]

            # test that modules were imported
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.pciids WHERE kernel_id=%s", (kernel_id,)
            )
            assert c.fetchone()[0] >= 50
            # test that firmware was imported
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.firmware WHERE kernel_id=%s", (kernel_id,)
            )
            assert c.fetchone()[0] >= 5
            # test that config was imported
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.kconfig WHERE kernel_id=%s", (kernel_id,)
            )
            assert c.fetchone()[0] >= 1000

    def test_register_firmware_file(self) -> None:
        with self.connman() as conn:
            c = conn.cursor()
            release = "test-release"
            architecture = "amd64"
            package = "quux"
            filename = "foo.fw"
            importer._register_firmware_file(
                c, release, architecture, package, filename
            )

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.firmware_packages WHERE filename=%s",
                (filename,),
            )
            assert c.fetchone()[0] == 1
            c.execute(
                "SELECT package FROM kinfodb.firmware_packages WHERE filename=%s",
                (filename,),
            )
            assert c.fetchone()[0] == package

    def test_import_firmware_packages(self) -> None:
        contents = ContentsDict(
            {
                "firmware-bnx2": [
                    "lib/firmware/bnx2/bnx2-mips-06-5.0.0.j3.fw",
                    "lib/firmware/bnx2/bnx2-mips-06-6.2.1.fw",
                ],
            }
        )
        with self.connman() as conn:
            release = "test-release"
            architecture = "amd64"
            importer.import_firmware_packages(conn, release, architecture, contents)

            c = conn.cursor()
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.firmware_packages WHERE filename=%s",
                ("bnx2/bnx2-mips-06-5.0.0.j3.fw",),
            )
            assert c.fetchone()[0] == 1
            c.execute(
                "SELECT package FROM kinfodb.firmware_packages WHERE filename=%s",
                ("bnx2/bnx2-mips-06-5.0.0.j3.fw",),
            )
            assert c.fetchone()[0] == "firmware-bnx2"

    @patch("subprocess.Popen.communicate", return_value=(data_contents_sample,))
    @patch("os.path.isfile", return_value=True)
    def test_import_contents(self, mock_isfile: Mock, mock_check_output: Mock) -> None:
        release = "test-release-import-contents"
        with self.connman() as conn:
            c = conn.cursor()
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.firmware_packages WHERE release=%(release)s",
                {"release": release},
            )
            assert c.fetchone()[0] == 0

            mock_check_output.reset_mock()
            mock_isfile.reset_mock()

            releases = [release]
            importer.import_contents(
                conn,
                releases,
                architectures=["amd64"],
                components=["main"],
                tree="/mock/path",
            )
            c = conn.cursor()

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.firmware_packages WHERE release=%(release)s",
                {"release": release},
            )
            # everything gets imported twice from the mock above:
            # once for amd64 and once for all.
            assert c.fetchone()[0] == 6

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.firmware_packages "
                "WHERE package=%(package)s AND release=%(release)s",
                {"package": "firmware-bnx2", "release": release},
            )
            assert c.fetchone()[0] == 6

    def test_register_pci_alias(self, caplog: pytest.LogCaptureFixture) -> None:
        with self.connman() as conn:
            c = conn.cursor()
            with self.temporary_kernel_id(c, release="test-importer") as kernel_id:

                pci = importer.PciAlias("*", "00005678", "", "", "", "", "")
                importer._register_pci_alias(c, kernel_id, "some-module-vid-wild", pci)
                c.execute(
                    "SELECT vid, did FROM kinfodb.pciids "
                    "WHERE module='some-module-vid-wild'",
                )
                data = c.fetchone()
                assert data[0] == "FFFFFFFF"
                assert data[1] == "00005678"

                pci = importer.PciAlias("00001234", "*", "", "", "", "", "")
                importer._register_pci_alias(c, kernel_id, "some-module-did-wild", pci)
                c.execute(
                    "SELECT vid, did FROM kinfodb.pciids "
                    "WHERE module='some-module-did-wild'",
                )
                data = c.fetchone()
                assert data[0] == "00001234"
                assert data[1] == "FFFFFFFF"

                with caplog.at_level(logging.ERROR):
                    pci = importer.PciAlias("1234", "5678", "", "", "", "", "")
                    importer._register_pci_alias(c, kernel_id, "some-module", pci)
                    assert "8 chars" in caplog.text

    def test_register_pci_device(self, caplog: pytest.LogCaptureFixture) -> None:
        with self.connman() as conn:
            c = conn.cursor()

            vid = "000018f6"
            did = "000098ab"
            vendor = "test-vendor"
            device = "test-device"

            importer._register_pci_device(c, vid, did, vendor, device)

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.pci_devices WHERE vendor=%s", (vendor,)
            )
            assert c.fetchone()[0] == 1

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.pci_devices WHERE did=%s", (did.upper(),)
            )
            assert c.fetchone()[0] == 1

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.pci_devices WHERE did=%s", ("aaaagggg",)
            )
            assert c.fetchone()[0] == 0

            with caplog.at_level(logging.ERROR):
                importer._register_pci_device(c, "1234", "5678", "", "")
                assert "8 chars" in caplog.text

    def test_import_pci_devices(self) -> None:
        with self.connman() as conn:
            importer.import_pci_devices(conn, pciid_sample.splitlines())

            c = conn.cursor()
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.pci_devices WHERE did=%s", ("0000163C",)
            )
            assert c.fetchone()[0] == 1

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.pci_devices WHERE vid=%s", ("000014E4",)
            )
            assert c.fetchone()[0] == 4

    def test_register_wiki_link(self) -> None:
        with self.connman() as conn:
            c = conn.cursor()

            module = "iwl3945"
            link = "iwlegacy"
            importer._register_wiki_link(c, module, link)

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.wiki_map WHERE module=%s", (module,)
            )
            assert c.fetchone()[0] == 1

            c.execute(
                "SELECT COUNT(*) FROM kinfodb.wiki_map WHERE module=%s",
                ("no_such_module",),
            )
            assert c.fetchone()[0] == 0

    def test_import_wiki_map(self) -> None:
        with self.connman() as conn:
            importer.import_wiki_map(conn, wiki_map_sample.splitlines())

            c = conn.cursor()
            c.execute("SELECT * FROM kinfodb.wiki_map")
            assert len(c.fetchall()) >= 2
            c.execute(
                "SELECT COUNT(*) FROM kinfodb.wiki_map WHERE module=%s", ("iwlwifi",)
            )
            assert c.fetchone()[0] == 1
