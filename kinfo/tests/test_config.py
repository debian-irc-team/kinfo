# Kernel and firmware info database tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

"""Unit test for kinfo/config.py"""

import logging
import os

from typing import Generator

import pytest

from kinfo.config import Config
from kinfo.tests.conftest import find_module_data


class TestConfig:
    @pytest.fixture(autouse=True)
    def testconf(self) -> Generator[str, None, None]:
        yield str(find_module_data("test_kinfo.conf"))

    def testNoFileName(self) -> None:
        """Test no specified filename to load"""
        with pytest.raises(ValueError):
            Config(skipDefaultFiles=True)

    def testGoodFileName(self, testconf: str) -> None:
        """Test loading of config file"""
        conf = Config(testconf)
        assert conf
        assert conf.db()["hostname"] == "test.example.net"

    def testBadFileName(self) -> None:
        """Test loading of non-existent config file"""
        with pytest.raises(IOError):
            Config(filename="/path/to/no/such/file")

    def testEnvironment(self, testconf: str) -> None:
        """Test loading config file from environment"""
        try:
            with pytest.raises(IOError):
                os.environ["KINFO_CONFIG"] = "/path/to/no/such/file"
                Config()

            os.environ["KINFO_CONFIG"] = testconf
            assert Config(skipDefaultFiles=True)

            with pytest.raises(ValueError):
                os.environ["KINFO_CONFIG"] = ""
                Config(skipDefaultFiles=True)

        finally:
            os.unsetenv("KINFO_CONFIG")

    def testConfDict(self) -> None:
        """Test loading config via a dict"""
        cd = {"database": "quux", "username": "foobar"}
        conf = Config(skipDefaultFiles=True, confdict=cd)
        assert conf
        assert conf.get("database", "database") == "quux"
        assert conf.get("database", "username") == "foobar"

    def testNoDatabaseConf(self, caplog: pytest.LogCaptureFixture) -> None:
        with caplog.at_level(logging.ERROR):
            cd = {"username": "foobar"}
            Config(skipDefaultFiles=True, confdict=cd)
            assert 'no database configuration' in caplog.text

    def testConfigGet(self, testconf: str) -> None:
        conf = Config(testconf)
        assert conf.get("database", "hostname", "fallback-value") == "test.example.net"
        assert conf.get("nosuchsection", "hostname", "fallback-value") == "fallback-value"
        assert conf.get("database", "nosuchkey", "fallback-value") == "fallback-value"

    def testBulkImporter(self, testconf: str) -> None:
        conf = Config(testconf)
        bi = conf.bulkimporter()

        assert bi.rsynchost
        assert bi.rsyncpath
        assert bi.cachebase

        assert len(bi.components) == 4
        assert "main" in bi.components
        assert len(bi.releases) > 5
        assert len(bi.architectures) >= 3
        assert "amd64" in bi.architectures

        assert "https" in bi.wikimap_url
        assert "https" in bi.pciid_url

        kf = bi.kernel_filters
        assert kf
        assert len(kf) >= 3
        assert kf["amd64"][0] == "^amd64$"
        assert kf["amd64"][1] == ""

    def testKernelFilters(self, testconf: str, caplog: pytest.LogCaptureFixture) -> None:
        conf = Config(testconf)

        s = "amd64,^cloud$,rt"
        kf = conf._unpack_kernel_filters(s)
        assert len(kf) == 1
        assert kf["amd64"][0] == "^cloud$"
        assert kf["amd64"][1] == "rt"

        s = """
            amd64,  ^cloud$,    rt
            arm64,  rpi,        banana
        """
        kf = conf._unpack_kernel_filters(s)
        assert len(kf) == 2
        assert kf["amd64"][0] == "^cloud$"
        assert kf["amd64"][1] == "rt"
        assert kf["arm64"][0] == "rpi"
        assert kf["arm64"][1] == "banana"

        for badval in ("", None):
            with caplog.at_level(logging.ERROR):
                conf._unpack_kernel_filters(badval)
                assert "no kernel_filters found" in caplog.text

        for badval in ("amd64", "amd64,foo"):
            with caplog.at_level(logging.ERROR):
                conf._unpack_kernel_filters(badval)
                assert "error in config item kernel_filters" in caplog.text
